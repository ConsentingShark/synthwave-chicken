using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundMapPlayer : MonoBehaviour {
	public SoundMap map;
	public int bufferSize;

	private AudioBuffer buffer;

	private void Awake() {
		buffer = new AudioBuffer(bufferSize, gameObject);
	}

	public void Play() {
		map.Play(buffer.Get());
	}

}
