using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSettingSetter : MonoBehaviour {
	public SoundSettings settings;
	public GroundType ground;
	private void Awake() {
		settings.GroundType = ground;
	}
}
