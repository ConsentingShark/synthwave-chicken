using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAudio : MonoBehaviour {
    [SerializeField] private SoundMap OnSelect;
    [SerializeField] private SoundMap OnClick;

    public static UIAudio Instance { get; private set; }

    private AudioBuffer buffer;

    private void Awake() {
        if (Instance == null) {
            Instance = this;
            transform.parent = null;
            DontDestroyOnLoad(gameObject);
        }
        else {
            Destroy(gameObject);
        }
    }

    private void Start() {
        buffer = new AudioBuffer(2, gameObject);
    }

    public void Click() {
        OnClick.Play(buffer.Get());
    }

    public void Select() {
        OnSelect.Play(buffer.Get());
    }

}
