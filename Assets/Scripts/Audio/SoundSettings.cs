using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GroundType {
	Steet = 0,
	Metal = 1
}

[CreateAssetMenu(fileName = "SoundSettings", menuName = "ScriptableObjects/Sound Settings")]
public class SoundSettings : ScriptableObject
{
	public GroundType GroundType;
}
