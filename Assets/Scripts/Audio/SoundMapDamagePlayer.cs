using Data;
using Data.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class SoundMapDamagePlayer : MonoBehaviour {
	[HideInInspector] [SerializeField]
	private SoundMap[] maps;
	private bool[] hasEntry;
	
	[SerializeField] private int bufferSize;

	private AudioBuffer buffer;
	private HealthComponent hc;

	[SerializeField] private float delay = 0;
	[SerializeField] private SoundMap death;

	private void Awake() {
		buffer = new AudioBuffer(bufferSize, gameObject);
		if (TryGetComponent(out hc)) {
			hc.OnDamage += Play;
		}
		else {
			Debug.LogError($"SoundMapDamagePlayer: Error locating HealthComponent on {gameObject.name}!");
		}
		hasEntry = new bool[maps.Length];

		for (int i = 0; i < maps.Length; ++i) {
			hasEntry[i] = maps[i] != null;
		}
	}

	public void Play(int _, bool died, DamageType damage) {
		int index = (int)damage;
		float dl = Random.Range(0, delay);

		// Play index zero and type if present
		if (hasEntry[0]) maps[0].Play(buffer.Get(), dl);
		if (index >= maps.Length) return;
		if (hasEntry[index]) {
			if (died) {
				var pos = transform.position;
				maps[index].SpawnSource(pos, dl);
				if (death != null) death.SpawnSource(pos);
			}
			else {
				maps[index].Play(buffer.Get(), dl);
			}
		}
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(SoundMapDamagePlayer))]
public class SoundMapDamagePlayerEditor : Editor {
	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		int[] values = (int[])System.Enum.GetValues(typeof(DamageType));

		SerializedProperty maps = serializedObject.FindProperty("maps");
		int max = Mathf.Max(values) + 1;
		if (maps.arraySize < max) maps.arraySize = max;

		foreach (DamageType t in values) {
			int i = (int)t;
			var map = maps.GetArrayElementAtIndex(i);
			EditorGUILayout.PropertyField(map, new GUIContent(System.Enum.GetName(typeof(DamageType), t)));
		}

		serializedObject.ApplyModifiedProperties();
	}
}
#endif
