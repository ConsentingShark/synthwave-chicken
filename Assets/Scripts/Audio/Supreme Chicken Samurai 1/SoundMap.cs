﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "SoundMap", menuName = "ScriptableObjects/SoundMap", order = 1)]
public class SoundMap : ScriptableObject {
    [System.Serializable]
    public struct Sound {
        public AudioClip clip;
        public float volumeReduction;
    }
    [SerializeField] private float spatialBlend = 1;
    [SerializeField] private Sound[] sounds;
    [SerializeField] private AudioMixerGroup mixer;
    [SerializeField] private float pitch = 1;
    [SerializeField] private float minDistance;

    public float SpatialBlend => spatialBlend;

    public AudioMixerGroup outputAudioMixerGroup => mixer;

    public Sound Get() {
        return sounds[Random.Range(0, sounds.Length)];
    }

    public AudioSource SpawnSource(Transform origin, float offset = 0) {
        return SpawnSource(origin.position, offset);
    }

    public AudioSource SpawnSource(Vector3 position, float offset = 0) {
        var sound = Get();
        var src = InstantiateAudioSourceAt(position);
        Play(src, offset);
        Destroy(src.gameObject, sound.clip.length + Time.fixedDeltaTime);
        return src;
    }

    public void Play(AudioSource source, float offset = 0) {
        SetupSource(source);
        if (offset > 0) {
            source.PlayDelayed(offset);
        }
        else {
            source.Play();
        }
    }

    public AudioSource PlayLoop(AudioSource source, float offset = 0) {
        Play(source, offset);
        source.loop = true;
        return source;
    }

    public AudioSource SpawnLoop(Vector3 position, float offset = 0) {
        var src = SpawnSource(position, offset);
        src.loop = true;
        return src;
    }

    public Sequence FadeLoop(Transform transform, float fadeInDuration, float fadeOutDuration) {
        return FadeLoop(transform.position, fadeInDuration, fadeOutDuration);
    }

    public Sequence FadeLoop(Vector3 position, float fadeInDuration, float fadeOutDuration) {
        AudioSource src = InstantiateAudioSourceAt(position);
        return FadeLoop(src, fadeInDuration, fadeOutDuration).AppendCallback(() => Destroy(src.gameObject));
    }

    public Sequence FadeLoop(AudioSource source, float fadeInDuration, float fadeOutDuration) {
        Sound s = SetupSource(source);
        source.volume = 0;
        source.loop = true;
        source.Play();
        source.DOFade(1 - s.volumeReduction, fadeInDuration);
        var seq = source.DOFade(0, fadeOutDuration);
        return DOTween.Sequence().Append(seq).Pause();
    }

    private Sound SetupSource(AudioSource source) {
        Sound s = Get();
        source.outputAudioMixerGroup = mixer;
        source.clip = s.clip;
        source.spatialBlend = spatialBlend;
        source.volume = 1 - s.volumeReduction;
        source.pitch = pitch;
        source.minDistance = minDistance;
        return s;
    }

    private static AudioSource InstantiateAudioSourceAt(Vector3 position) {
        GameObject go = new GameObject();
        go.transform.position = position;
        return go.AddComponent<AudioSource>();
    }
}
