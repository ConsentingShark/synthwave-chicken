using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleSoundMapFade : MonoBehaviour {
	public SoundMap map;
	public float fadeIn;
	public float fadeOut;

	private Sequence seq;
	private void Start() {
		seq = map.FadeLoop(transform.position, fadeIn, fadeOut);
	}

	private void OnDestroy() {
		seq.Play();
	}
}
