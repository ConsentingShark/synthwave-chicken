using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLightHook : MonoBehaviour {
	[SerializeField] private GameObject[] controlledObjects;
	[SerializeField] private Light[] controlledLights;

	[SerializeField] private bool flag;

	private void OnPreCull() {
		foreach (var go in controlledObjects) {
			go.SetActive(flag);
		}
		foreach (var light in controlledLights) {
			light.enabled = flag;
		}
	}

	private void OnPreRender() {
		foreach (var go in controlledObjects) {
			go.SetActive(flag);
		}
		foreach (var light in controlledLights) {
			light.enabled = flag;
		}
	}
	private void OnPostRender() {
		foreach (var go in controlledObjects) {
			go.SetActive(!flag);
		}
		foreach (var light in controlledLights) {
			light.enabled = !flag;
		}
	}

}
