using UI;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(menuName = "ScriptableObjects/PlaySettings", fileName = "PlaySettings")]
    public class PlaySettings : ScriptableObject
    {
        public LevelCollection allLevel;
        public LevelPreview currentLevel;
        
        public bool isStoryPlaythrough;
        public float currentTime = 0f;
    }
}
