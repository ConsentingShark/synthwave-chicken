using System;
using System.Collections.Generic;
using Data.Player;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem.Controls;

namespace Data
{
    public class HealthComponent : MonoBehaviour
    {
        public delegate void DamageDelegate(int damage, bool died, DamageType damageType);
        public delegate void KnockbackDelegate(Vector3 origin, float strength);

        public int health;
        
        [SerializeField] public HealthSettings settings;
        private TeamType Team { get; set; }

        [SerializeField] public HealthCounter optionalHealthCounter = null;
        
        public event DamageDelegate OnDamage;
        public event KnockbackDelegate OnKnockback;

        public UnityEvent OnDamageUnity;

        private Rigidbody rb;

        [SerializeField] private bool knockbackToRigidbody = true;
        [SerializeField] private bool destroyOnDeath = false;

        private void Start()
        {
            if (settings != null) {
                health = settings.startHealth;
                Team = settings.team;
            }
            else {
                Debug.LogWarning($"Possible Error: No HealtSetting on HealthComponent of {gameObject.name}. Falling back to Team Object.");
                Team = TeamType.Object;
            }
            rb = GetComponent<Rigidbody>();
            if (optionalHealthCounter != null) optionalHealthCounter.settings = settings;
            UpdatePlayerHealth();
        }

        public void CutsceneDealDamage(int value) {
            DealDamage(value);
        }

        public (bool, bool) DealDamage(int value) {
            return DealDamage(value, DamageType.Unset);
        }

        public (bool, bool) DealDamage(int value, DamageType damageType)
        {
            return DealDamage(value, Team, damageType);
        }

        /// <summary>
        /// returns (Damage dealt, has died)
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetTeams"></param>
        /// <returns>(Damage dealt, has died)</returns>
        public (bool, bool) DealDamage(int value, TeamType targetTeams, DamageType damageType = DamageType.Unset)
        {
            if (!enabled) return (false, false);
            if (value < 0)
            {
                value = 0;
            }

            if ((targetTeams & Team) != 0)
            {
                int truedmg = Mathf.Min(health, value);
                health -= truedmg;

                UpdatePlayerHealth();

                OnDamageUnity.Invoke();

                if (health == 0) {
                    OnDamage?.Invoke(value, true, damageType);
                    if (destroyOnDeath) Destroy(this);
                    return (true, true);
                }
                OnDamage?.Invoke(value, false, damageType);
                return (true, false);
            }

            return (false, false);
        }

        public void KnockBack(Vector3 origin, float strength = 1) {
            if (!enabled) return;
            if (knockbackToRigidbody) {
                rb.AddForce((transform.position - origin).normalized * strength, ForceMode.Impulse);
            }
            OnKnockback?.Invoke(origin, strength);
        }

        public void Heal(int value)
        {
            Heal(value, Team);
        }

        public void Heal(int value, TeamType targetTeams)
        {
            if (!enabled) return;
            if (value < 0)
            {
                value = 0;
            }

            if ((targetTeams & Team) != 0)
            {
                int trueheal = Mathf.Min(settings.maxHealth-health, value);
                health += trueheal;

                UpdatePlayerHealth();
            }
        }

        // Updates a PlayerHealth Scriptable Object for the Lifebar if available
        public void UpdatePlayerHealth()
        {
            if (optionalHealthCounter != null)
            {
                optionalHealthCounter.health = health;
            }
        }
    }
}