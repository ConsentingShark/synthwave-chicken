using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DamageType {
	Unset = 0,
	Sword = 1,
	Blaster = 2,
	Laser = 3,
	Blunt = 4,
}
