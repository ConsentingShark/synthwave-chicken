﻿using UnityEngine;

namespace Data
{
    [CreateAssetMenu (fileName = "healthData", menuName = "ScriptableObjects/HealthData")]
    public class HealthSettings : ScriptableObject
    {
        public int maxHealth;
        public int startHealth;

        public TeamType team;
    }
}