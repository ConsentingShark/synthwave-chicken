using UnityEngine;

namespace Data.Player
{
    [CreateAssetMenu (fileName = "HealthCounter", menuName = "ScriptableObjects/Player/Health Counter")]
    public class HealthCounter : ScriptableObject
    {
        public HealthSettings settings;
        public int health;
    }
}
