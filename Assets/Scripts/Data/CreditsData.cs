using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Credits", fileName = "NameCredits")]
public class CreditsData : ScriptableObject
{
    public String name;
    public String department;
    public List<String> sideJoke;
}
