using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerStats", menuName = "ScriptableObjects/Player/Player Stats")]
public class PlayerStats : ScriptableObject {
	public float meleeKnockback;
	public float gunKnockback;

	public int meleeDamage;
	public int gunDamage;

	public int gunHeat;
}
