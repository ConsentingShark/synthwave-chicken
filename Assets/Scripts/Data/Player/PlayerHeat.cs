using System;
using UnityEngine;

namespace Data.Player
{
    [CreateAssetMenu (fileName = "playerHeat", menuName = "ScriptableObjects/Player/Player Heat")]
    public class PlayerHeat : ScriptableObject
    {
        public int maxHeat;
        public int heat;
        public bool overheated;

        public float timeForCooldown;
        public float timeForCooldownOverheated;
        [Range(0,.2f)]
        public float cooldownRate;
        
        private float _lastTimeHeatIncreased;

        public void OnEnable()
        {
            overheated = false;
            heat = 0;
            _lastTimeHeatIncreased = Time.time;
        }

        public void UpdateHeatCooldown()
        {
            if (Time.time - _lastTimeHeatIncreased > timeForCooldown)
            {
                heat = Mathf.Max(heat - 1, 0);
                _lastTimeHeatIncreased += cooldownRate;

                if (heat == 0) {
                    overheated = false;
                }
            }
        }
        
        public void IncreaseHeat(int value)
        {
            if (value < 0)
            {
                return;
            }
            
            heat += value;
            if (heat >= maxHeat)
            {
                OverHeat();
            }
            else
            {
                overheated = false;
                _lastTimeHeatIncreased = Time.time;
            }

        }

        public void ResetHeat()
        {
            heat = 0;
            overheated = false;
            _lastTimeHeatIncreased = Time.time - timeForCooldown;
        }

        public void OverHeat()
        {
            heat = maxHeat;
            overheated = true;
            _lastTimeHeatIncreased = Time.time - (timeForCooldown - timeForCooldownOverheated);
        }
    }
}
