using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class MobSpawner : MonoBehaviour
{
    public float animTime;
    public float maxSize;
    public GameObject cookPrefab;
    public bool spawn = true;
    private BossBehaviour _boss;
    [SerializeField] private SoundMap fx;
    public Action<GameObject> ProcessSpawned = null;

    void Start()
    {
        StartCoroutine(WaitAndSpawn(animTime));
    }

    private IEnumerator WaitAndSpawn(float time)
    {
        fx.SpawnSource(transform.position);
        transform.DOScale(new Vector3(maxSize, transform.localScale.y, maxSize), animTime / 4);
        yield return new WaitForSeconds(animTime / 4);
        yield return new WaitForSeconds(animTime / 2);
        transform.DOScale(new Vector3(0, transform.localScale.y, 0), animTime / 4);

        if (spawn) {
            GameObject zerg = Instantiate(cookPrefab, transform.position, Quaternion.identity);
            bool isZerg = zerg.TryGetComponent(out Zergling_Controller ctrl);
            if (isZerg) ctrl.aggroRange = 100;
            if (_boss != null) Physics.IgnoreCollision(zerg.GetComponent<Collider>(), _boss.GetComponent<Collider>());
            if (ProcessSpawned != null) ProcessSpawned(zerg);
        }

        yield return new WaitForSeconds(animTime / 4);
        Destroy(gameObject);
    }

    public void SetBoss(BossBehaviour b)
    {
        _boss = b;
    }
}
