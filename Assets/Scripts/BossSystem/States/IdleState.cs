using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IState
{
    private BossBehaviour _boss;
    private Rigidbody _rigidbody;
    private BossSettings _settings;
    private float _waitTime;
    private float _timeToWait;

    public IdleState(BossBehaviour boss)
    {
        _boss = boss;
        _rigidbody = boss.GetComponent<Rigidbody>();
        _settings = boss.settings;
        _timeToWait = _settings.idleWaitTime;
    }

    public void Enter()
    {
        //Debug.Log("Enter Idle");
        _boss.model.GetComponent<Renderer>().material.color = Color.red;
        _waitTime = 0;
    }

    public void Run()
    {
        _rigidbody.velocity = Vector3.zero;
        _waitTime += Time.fixedDeltaTime;
        if(_waitTime > _timeToWait)
        {
            _boss.model.GetComponent<Renderer>().material.color = Color.white;
            _boss.ChangeState(BossState.PrepareCharge);
        }
        /*
        else
            _boss.LookInDirection(_boss.GetPlayerDirection());
        */
    }

    public void Exit()
    {
        _boss.RotateTowards(_boss.GetPlayerDirection());
    }
}