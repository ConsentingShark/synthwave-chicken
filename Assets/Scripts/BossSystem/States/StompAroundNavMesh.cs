using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StompAroundNavMesh : IState
{
    private BossBehaviour _boss;
    private BossSettings _settings;
    private Rigidbody _rigidbody;
    private Vector3 _targetPos;
    private Vector3 _currentDirection;
    private int _stompCounter;
    private float _distanceToWalk;
    private NavMeshAgent _agent;

    public StompAroundNavMesh(BossBehaviour boss)
    {
        _boss = boss;
        _agent = boss.GetComponent<NavMeshAgent>();
        _settings = boss.settings;
        _rigidbody = _boss.GetComponent<Rigidbody>();
    }

    public void Enter()
    {
        _agent.isStopped = false;
        _boss.EnableStompBox(true);
        //Debug.Log("Enter Stomp Around");
        _stompCounter = 0;
        _distanceToWalk = 0;
        SetNextTarget();
    }

    public void Run()
    {
        if(_stompCounter > _settings.stompTargets.Count)
        {
            _rigidbody.velocity = Vector3.zero;
            _boss.NextPatternState();
        }
        else if(CheckIfWalkedDistance())
        {
            if(_stompCounter < _settings.stompTargets.Count)
                SetNextTarget();
            else
            {
                _stompCounter++;
            }
        }

        /*
        if(_stompCounter > _settings.stompTargets.Count)
        {
            _rigidbody.velocity = Vector3.zero;
            _boss.NextPatternState();
        }
        else if(!CheckIfWalkedDistance())
        {
            MoveTowardsTarget();
        }
        else
        {
            //_rigidbody.velocity = Vector3.zero;

            if(_stompCounter < _settings.stompTargets.Count)
                SetNextTarget();
            else
            {
                _stompCounter++;
            }
        }
        */
    }

    public void Exit()
    {
        _boss.EnableStompBox(false);
        _agent.isStopped = true;
    }

    private void SetNextTarget()
    {
        Vector2 target = _settings.stompTargets[_stompCounter];
        _targetPos = new Vector3(target.x, _boss.transform.position.y, target.y);
        _currentDirection = (_targetPos - _boss.transform.position).normalized;
        _distanceToWalk = Vector3.Distance(_boss.transform.position, _targetPos);

        NavMeshPath navMeshPath = new NavMeshPath();
        if (_agent.CalculatePath(_targetPos, navMeshPath) && navMeshPath.status == NavMeshPathStatus.PathComplete)
        {
            _agent.SetDestination(_targetPos);
            _stompCounter++;
        }
        else
            _boss.NextPatternState();

        //_boss.RotateTowards(_currentDirection);
    }

    /*
    private void SetNextTarget()
    {
        Vector2 target = _settings.stompTargets[_stompCounter];
        _targetPos = new Vector3(target.x, _boss.transform.position.y, target.y);
        _currentDirection = (_targetPos - _boss.transform.position).normalized;
        _distanceToWalk = Vector3.Distance(_boss.transform.position, _targetPos);
        _boss.RotateTowards(_currentDirection);
        _stompCounter++;
    }
    */

    /*
    private void MoveTowardsTarget()
    {
        //Vector3 toWalk = _currentDirection * _boss.settings.stompSpeed * Time.deltaTime;
        //_rigidbody.MovePosition(_boss.transform.position + toWalk);
        _currentDirection = (_targetPos - _boss.transform.position).normalized;
        _rigidbody.velocity = _currentDirection * Time.deltaTime * _settings.stompSpeed;
        _distanceToWalk = Vector3.Distance(_boss.transform.position, _targetPos);
    }
    */

    private bool CheckIfWalkedDistance()
    {
        _distanceToWalk = Vector3.Distance(_boss.transform.position, _targetPos);
        return _distanceToWalk < 1f;
    }
}