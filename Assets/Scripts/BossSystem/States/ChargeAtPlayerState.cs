using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeAtPlayerState : IState
{
    private BossBehaviour _boss;
    private BossSettings _settings;
    private Rigidbody _rigidbody;
    private bool _charging;
    public int chargeCounter;

    public ChargeAtPlayerState(BossBehaviour boss)
    {
        chargeCounter = 0;
        _boss = boss;
        _settings = boss.settings;
        _rigidbody = boss.GetComponent<Rigidbody>();
    }

    public void Enter()
    {
        _boss.trails.SetActive(true);
        _boss.SetAnimState(BossAnimState.Charging, true);
        _boss.EnableStompBox(true);

        if(chargeCounter < _settings.chargeCount)
        {
            Charge();
            chargeCounter++;
        }
        else
        {
            chargeCounter = 0;
            _boss.NextPatternState();
        }
    }

    public void Run()
    {
        if (_charging && _rigidbody.velocity.magnitude < 1f)
        {
            _charging = false;
            _boss.ChangeState(BossState.AfterCharge);
        }
    }

    public void Exit()
    {
        _boss.SetAnimState(BossAnimState.Charging, false);
        _boss.EnableStompBox(false);
    }

    private void Charge()
    {
        Vector3 direction = _boss.GetPlayerDirection();
        _boss.LookInDirection(direction);
        //_boss.RotateTowards(direction);

        //_rigidbody.AddForce(direction * _settings.chargeForce, ForceMode.Force);
        _rigidbody.velocity = direction * _settings.chargeForce;
        _charging = true;
    }
}