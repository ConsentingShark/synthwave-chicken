using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StompAroundState : IState
{
    private BossBehaviour _boss;
    private BossSettings _settings;
    private Rigidbody _rigidbody;
    private Vector3 _targetPos;
    private Vector3 _currentDirection;
    private int _stompCounter;
    private float _distanceToWalk;

    public StompAroundState(BossBehaviour boss)
    {
        _boss = boss;
        _settings = boss.settings;
        _rigidbody = _boss.GetComponent<Rigidbody>();
    }

    public void Enter()
    {
        _boss.SetAnimState(BossAnimState.Walking, true);
        _boss.SpawnZerglings(_settings.cooksToSpawn);
        _boss.EnableStompBox(true);
        //Debug.Log("Enter Stomp Around");
        _stompCounter = 0;
        _distanceToWalk = 0;
        SetNextTarget();
    }

    public void Run()
    {
        if(_stompCounter > _settings.stompTargets.Count)
        {
            _rigidbody.velocity = Vector3.zero;
            _boss.NextPatternState();
        }
        else if(!CheckIfWalkedDistance())
        {
            MoveTowardsTarget();
        }
        else
        {
            //_rigidbody.velocity = Vector3.zero;

            if(_stompCounter < _settings.stompTargets.Count)
                SetNextTarget();
            else
            {
                _stompCounter++;
            }
        }
    }

    public void Exit()
    {
        _boss.SetAnimState(BossAnimState.Walking, false);
        _boss.EnableStompBox(false);
    }

    private void SetNextTarget()
    {
        Vector2 target = _settings.stompTargets[_stompCounter];
        _targetPos = new Vector3(target.x, _boss.transform.position.y, target.y);
        _currentDirection = (_targetPos - _boss.transform.position).normalized;
        _distanceToWalk = Vector3.Distance(_boss.transform.position, _targetPos);
        _boss.RotateTowards(_currentDirection);
        _stompCounter++;
    }

    private void MoveTowardsTarget()
    {
        //Vector3 toWalk = _currentDirection * _boss.settings.stompSpeed * Time.deltaTime;
        //_rigidbody.MovePosition(_boss.transform.position + toWalk);
        _currentDirection = (_targetPos - _boss.transform.position).normalized;
        _rigidbody.velocity = _currentDirection * Time.fixedDeltaTime * _settings.stompSpeed;
        _distanceToWalk = Vector3.Distance(_boss.transform.position, _targetPos);
    }

    private bool CheckIfWalkedDistance()
    {
        return _distanceToWalk < 1f;
    }
}