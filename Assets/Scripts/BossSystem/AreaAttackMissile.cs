using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AreaAttackMissile : MonoBehaviour
{
    public GameObject missile;
    public GameObject sparks;
    public GameObject decal;
    public float decalRotateSpeed = 1f;
    private BossSettings _settings;
    private bool _jittering;

    private bool _louder;
    private bool _lower;

    private float _growTime;
    public Vector2 maxSize;
    private Renderer _missileRenderer;
    private AudioSource _missileAudio;
    [SerializeField] private SoundMap sound;

    private void Start()
    {
        _missileRenderer = missile.GetComponent<Renderer>();
        _missileAudio = missile.GetComponent<AudioSource>();
        //decal.transform.rotation = Quaternion.Euler(0, Random.Range(0,180), 0);
        decal.GetComponent<DecalController>().Action();
    }

    private void Update()
    {
        decal.transform.Rotate(new Vector3(0, Time.deltaTime * decalRotateSpeed, 0), Space.Self);

        if(_jittering)
        {
            _missileRenderer.material.SetVector("_EmissionColor", Color.red * Random.Range(3.5f,6f));
        }
        //if(_louder)
        //{
        //    _missileAudio.volume += Time.deltaTime * 4;
        //}
        //if(_lower)
        //{
        //    _missileAudio.volume -= Time.deltaTime * 4;
        //}
    }

    public void StartAttack()
    {
        StartCoroutine(WarnAndSpawn());
    }

    private IEnumerator WarnAndSpawn()
    {
        yield return new WaitForSeconds(_settings.warningDuration);
        missile.SetActive(true);
        sparks.SetActive(true);
        decal.SetActive(false);
        _jittering = true;
        _louder = true;
        var seq = sound.FadeLoop(_missileAudio, _growTime, _growTime);
        missile.transform.DOScale(new Vector3(maxSize.x, missile.transform.localScale.y, maxSize.y), _growTime);
        yield return new WaitForSeconds(_growTime);
        _louder = false;
        yield return new WaitForSeconds(_growTime * 2);
        _lower = true;
        missile.transform.DOScale(new Vector3(0, missile.transform.localScale.y, 0), _growTime);
        seq.Play();
        yield return new WaitForSeconds(_growTime);
        _lower = false;
        _jittering = false;
        Destroy(gameObject);
    }

    public void SetSettings(BossSettings s)
    {
        _settings = s;
        _growTime = _settings.attackDuration / 4;
    }
}
