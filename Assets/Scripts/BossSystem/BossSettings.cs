using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "BossSettings", menuName = "ScriptableObjects/BossSettings")]
public class BossSettings : ScriptableObject
{
    public BossState startState;
    public List<BossState> pattern;
    public float waitBetweenStates = 1f;
    public int hitDamage = 5;
    public float knockBackForce = 1f;
    public float knockBackDamageCooldown = 1f;
    //public Vector3 areaCenter;
    //public Vector3 areaSize;
    public float cookSpawnHeight;
    public float minWaitTimeCocks;
    public float maxWaitTimeCocks;

    [Header("Borders")]
    public Vector3 centerPoint;
    public GameObject laserSpawnArea;
    public GameObject cookSpawnArea;
    public GameObject dinoWalkArea;

    [Header("Start")]
    public float waitToStart;

    [Header("Idle")]
    public float idleWaitTime = 5f;

    [Header("Charge At Player")]
    [Range(0f, 100f)]
    public float chargeForce = 10f;
    [Range(0.1f, 5f)]
    public float chargeWaitTime = 3f;
    [Range(1, 10)]
    public int chargeCount = 3;

    [Header("Stomp Around")]
    public bool showTargets = true;
    [Range(100f, 1000f)]
    public float stompSpeed = 100f;
    [Range(0.1f, 1f)]
    public float stompTargetDistance = 0.1f;
    public List<Vector2> stompTargets;
    public int cooksToSpawn;

    [Header("Shoot Area")]
    public bool showArea = true;
    public LayerMask checkLaserMask;
    [Range(0f,1f)]
    public float targetPlayerChance;
    [Range(1, 1000)]
    public int shootAmount = 5;
    public Vector3 shootAreaSize;
    public Vector3 shootAreaClamp;
    [Range(0f,1f)]
    public float minWaitBetweenSpawn = 0.1f;
    [Range(0.1f,1.1f)]
    public float maxWaitBetweenSpawn = 0.3f;
    [Range(0.1f, 5f)]
    public float warningDuration = 2f;
    public float attackDuration = 1f;
    public GameObject areaAttackPrefab;
    public int damagePerSecondArea = 1;

    [Header("Range Attack")]
    public GameObject rangeAttackPrefab;
    [Range(10,180)]
    public float rangeAttackDegrees = 90f;
    public float rotateSpeed = 10f;
    public int damagePerSecondRange = 1;
}
