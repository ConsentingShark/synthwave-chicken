using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BossState
{
    Start,
    Idle,
    ChargeAtPlayer,
    RangeAttack,
    ShootArea,
    StompAround,
    PrepareCharge,
    PrepareRange,
    AfterCharge
}

public interface IState
{
    void Enter();
    void Run();
    void Exit();
}

public class StateMachine
{
    // Current
    private IState _currentState;
    public BossState currentState;

    // States
    public StartState startState;
    public IdleState idleState;
    public ChargeAtPlayerState chargeAtPlayerState;
    public RangeAttackState rangeAttackState;
    public ShootAreaState shootAreaState;
    public StompAroundState stompAroundState;
    public PrepareChargeState prepareChargeState;
    public PrepareRangeState prepareRangeState;
    public StompAroundNavMesh stompAroundNavMesh;
    public AfterChargeState afterChargeState;

    private bool _stop;

    public void InitStates(BossBehaviour boss)
    {
        //Instantiate States
        startState = new StartState(boss);
        idleState = new IdleState(boss);
        chargeAtPlayerState = new ChargeAtPlayerState(boss);
        rangeAttackState = new RangeAttackState(boss);
        shootAreaState = new ShootAreaState(boss);
        stompAroundState = new StompAroundState(boss);
        prepareChargeState = new PrepareChargeState(boss);
        prepareRangeState = new PrepareRangeState(boss);
        stompAroundNavMesh = new StompAroundNavMesh(boss);
        afterChargeState = new AfterChargeState(boss);

        //Set Start State
        ChangeState(boss.settings.startState);
    }

    public void ChangeState(BossState state)
    {
        // Call Exit
        if(_currentState != null)
            _currentState.Exit();

        // Set Current
        currentState = state;

        switch(state)
        {
            case BossState.Start:
                _currentState = startState;
                break;
            case BossState.ChargeAtPlayer:
                _currentState = chargeAtPlayerState;
                break;
            case BossState.Idle:
                _currentState = idleState;
                break;
            case BossState.RangeAttack:
                _currentState = rangeAttackState;
                break;
            case BossState.ShootArea:
                _currentState = shootAreaState;
                break;
            case BossState.StompAround:
                _currentState = stompAroundState;
                break;
            case BossState.PrepareCharge:
                _currentState = prepareChargeState;
                break;
            case BossState.PrepareRange:
                _currentState = prepareRangeState;
                break;
            case BossState.AfterCharge:
                _currentState = afterChargeState;
                break;
            default:
                Debug.LogWarning("Boss State not implemented: " + state);
                break;
        }

        // Call Enter
        _currentState.Enter();
    }

    public void Execute()
    {
        // Call Run
        if(_currentState != null && !_stop)
            _currentState.Run();
    }

    public void Stop()
    {
        _stop = true;
    }
}