using Data;
using Drops;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Pickups
{
    public class HealthDropComponent : MonoBehaviour
    {
        [SerializeField] public HealthDropData healthDropData;
        [SerializeField] public HealthComponent healthComponent;

        private bool _isDropped = false;
        
        public void OnEnable()
        {
            healthComponent.OnDamage += OnDamage;
        }

        public void OnDisable()
        {
            healthComponent.OnDamage -= OnDamage;
        }

        public void OnDamage(int damage, bool died, DamageType _ = DamageType.Unset)
        {
            if (died && !_isDropped)
            {
                if (Random.Range(0, 100) <= healthDropData.dropChance)
                {
                    GameObject healthDrop = Instantiate(healthDropData.healthDropPrefab);
                    healthDrop.transform.position = transform.position;
                    healthDrop.GetComponent<Rigidbody>().AddForce(Vector3.up * 10, ForceMode.Impulse);
                    healthDrop.GetComponentInChildren<ParticleSystem>().Play();
                }

                _isDropped = true;
            }
        }
    }
}
