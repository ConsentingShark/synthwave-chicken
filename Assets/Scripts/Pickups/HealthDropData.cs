using UnityEngine;

namespace Drops
{
    [CreateAssetMenu(menuName = "ScriptableObjects/HealthDrop Data", fileName = "HealthDropData")]
    public class HealthDropData : ScriptableObject
    {
        
        public float dropChance;
        public int healValue;
        public GameObject healthDropPrefab;
        public float dropCooldown = 0.5f;
        
        [Header("Magnetic Follow")]
        public float followStartRange = 7.5f;
        public float pickUpRange = 2;
        public float maxSpeed = 50;
        public float acceleration = 1000;
        

    }
}
