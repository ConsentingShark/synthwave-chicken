using Data;
using UnityEngine;

namespace Pickups
{
    public class DropComponent : MonoBehaviour
    {
        [SerializeField] public HealthComponent healthComponent;
        [SerializeField] public GameObject prefab;
        [SerializeField] public float heightSpawn;
        [SerializeField] public bool canDropMultipleTimes = false;
        
        private bool _isDropped = false;
        
        public void OnEnable()
        {
            healthComponent.OnDamage += OnDamage;
        }

        public void OnDisable()
        {
            healthComponent.OnDamage -= OnDamage;
        }

        public void OnDamage(int damage, bool died, DamageType _ = DamageType.Unset)
        {
            if (died && (!_isDropped || canDropMultipleTimes))
            {
                GameObject drop = Instantiate(prefab);
                drop.transform.position = new Vector3(transform.position.x, heightSpawn, transform.position.z);;
                _isDropped = true;
            }
        }
    }
}
