using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public interface ChickenInput {
	public void UpdateMoveControl(InputAction.CallbackContext x);

	public void UpdateCursorPosition(InputAction.CallbackContext x);

	public void UpdateAimDirection(InputAction.CallbackContext x);

	public void UpdateAttackCommand(InputAction.CallbackContext x);

	public void UpdateShootCommand(InputAction.CallbackContext x);

	public void UpdateControlScheme(PlayerInput input);

	public void UpdateInteract(InputAction.CallbackContext x);
}

public interface DialogueInput {
	void NextSentence(InputAction.CallbackContext x);
}

public interface PauseInput {
	void PauseGame();
	void ResumeGame();
	void Restart();
}

public class InputManager : MonoBehaviour, DialogueInput, PauseInput, ChickenInput {
	public PauseInput pauseInput;
	public DialogueInput dialogueInput;
	public ChickenInput chickenInput;

	private PlayerInput pi;

	public static InputManager Instance { get; private set; }

	public string CurrentControlScheme => pi.currentControlScheme;

	private void Awake() {
		Instance = this;
		pi = GetComponent<PlayerInput>();
	}

	public bool PlayerUseGamePad() {
		return pi.currentControlScheme == "GamePad";
	}

	public void DisablePlayerControl() {
		if (!PlayerControlEnabled()) return;
		pi.SwitchCurrentActionMap("Menuing");
	}

	public void EnablePlayerControl() {
		if (PlayerControlEnabled()) return;
		pi.SwitchCurrentActionMap("Player");
	}

	public void EnableDialogueControl() {
		if (pi.currentActionMap.name == "Dialogue") return;
		pi.SwitchCurrentActionMap("Dialogue");
	}

	public bool PlayerControlEnabled() {
		return pi.currentActionMap.name == "Player";
	}

	public void NextSentence(InputAction.CallbackContext x) {
		dialogueInput.NextSentence(x);
	}

	public void Restart() {
		pauseInput.Restart();
	}

	public void UpdateAimDirection(InputAction.CallbackContext x) {
		chickenInput.UpdateAimDirection(x);
	}

	public void UpdateAttackCommand(InputAction.CallbackContext x) {
		chickenInput.UpdateAttackCommand(x);
	}

	public void UpdateControlScheme(PlayerInput input) {
		chickenInput.UpdateControlScheme(input);
	}

	public void UpdateCursorPosition(InputAction.CallbackContext x) {
		chickenInput.UpdateCursorPosition(x);
	}

	public void UpdateMoveControl(InputAction.CallbackContext x) {
		chickenInput.UpdateMoveControl(x);
	}

	public void UpdateShootCommand(InputAction.CallbackContext x) {
		chickenInput.UpdateShootCommand(x);
	}

	public void PauseGame() {
		pauseInput.PauseGame();
	}

	public void ResumeGame() {
		pauseInput.ResumeGame();
	}

	public void UpdateInteract(InputAction.CallbackContext x) {
		chickenInput.UpdateInteract(x);
	}
}
