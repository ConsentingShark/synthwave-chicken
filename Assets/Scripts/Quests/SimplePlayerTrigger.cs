using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class SimplePlayerTrigger : MonoBehaviour {
	[FormerlySerializedAs("OnTrigger")]
	[SerializeField] private UnityEvent OnTriggerEnterEvent;

	[SerializeField] private UnityEvent OnTriggerExitEvent;

	[SerializeField] public bool triggerEnabled = true;

	public void SetTriggerEnabled(bool b) {
		triggerEnabled = b;
	}

	private void OnTriggerEnter(Collider other) {
		if (!triggerEnabled) return;
		if (other.CompareTag("Player")) {
			OnTriggerEnterEvent.Invoke();
		}
	}

	private void OnTriggerExit(Collider other) {
		if (!triggerEnabled) return;
		if (other.CompareTag("Player")) {
			OnTriggerExitEvent.Invoke();
		}
	}

	public void Destroy(bool destroyGameObject) {
		if (destroyGameObject) Destroy(gameObject);
		else Destroy(this);
	}
}
