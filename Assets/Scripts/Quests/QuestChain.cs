using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Quest/Chain", fileName = "QuestChain")]
public class QuestChain : ScriptableObject {
	public QuestObejctive[] objectives;
	public int currentObejtiveIndex;

	public delegate void QuestChainDelegate(QuestObejctive obejctive);

	public event QuestChainDelegate OnNewQuestObjective;
	public event QuestChainDelegate OnQuestObjectiveUpdate;

	public QuestObejctive currentObjetive => objectives[currentObejtiveIndex];

	public bool Tracking { get; private set; }

	public void ResetProgress() {
		//Debug.Log($"Chain Reset: {name}");

		foreach (var quest in objectives) {
			quest.ResetProgress();
		}
		currentObejtiveIndex = 0;
	}

	public void StartTracking() {
		//Debug.Log($"Chain Start: {name}");
		currentObjetive.OnProgress += HandleQuestProgress;
		OnNewQuestObjective?.Invoke(currentObjetive);

		Tracking = true;
	}

	public void StopTracking() {
		currentObjetive.OnProgress -= HandleQuestProgress;

		Tracking = false;
	}

	private void HandleQuestProgress() {
		//Debug.Log($"Chain Progress: {name}");

		OnQuestObjectiveUpdate?.Invoke(currentObjetive);
		if (!currentObjetive.Completed) {
			return;
		}

		currentObjetive.OnProgress -= HandleQuestProgress;

		++currentObejtiveIndex;

		if (currentObejtiveIndex >= objectives.Length) {
			OnNewQuestObjective?.Invoke(null);
		}

		else {
			currentObjetive.OnProgress += HandleQuestProgress;
			OnNewQuestObjective?.Invoke(currentObjetive);
		}


	}
}
