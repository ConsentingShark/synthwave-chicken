using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;

public class QuestChainController : MonoBehaviour {
	[SerializeField] private QuestChain questChain;

	[SerializeField] public bool TrackOnAwake = true;

	private TextMeshProUGUI objectiveText;

	private StringBuilder sb = new StringBuilder(256);

	// Animation
	[SerializeField] private float textMoveOffsetX = -50;
	[SerializeField] private float textMoveDuration = 0.3f;
	[SerializeField] private float characterWriteDuration;
	[SerializeField] private float questChangeDelay;

	private float objTextLocalX;

	private string Text { get => objectiveText.text; set => objectiveText.text = value; }

	//private Sequence currentSequence;
	private SequenceQueue sequences = new SequenceQueue();

	private AudioSource src;

	[SerializeField] private SoundMap writeTextSound;
	[SerializeField] private float writeTextSoundFrequency = 1;

	private float summedWriteTextSoundFrequency;

	private int prevtexlen;

	private void Start() {
		objectiveText = GameObject.FindGameObjectWithTag("Quest Text").GetComponent<TextMeshProUGUI>();
		Text = "";
		questChain.ResetProgress();

		objTextLocalX = objectiveText.rectTransform.localPosition.x;

		questChain.OnNewQuestObjective += HandleNewQuest;
		questChain.OnQuestObjectiveUpdate += HandleQuestProgress;

		if (TrackOnAwake) questChain.StartTracking();
		src = gameObject.AddComponent<AudioSource>();
	}

	private void OnDisable() {
		questChain.OnNewQuestObjective -= HandleNewQuest;
		questChain.OnQuestObjectiveUpdate -= HandleQuestProgress;
	}

	private void HandleQuestProgress(QuestObejctive quest) {
		QuestLog($"Quest Controller: Progress - {quest.name}");
		sequences += DOTween.Sequence().AppendCallback(() => {
			Text = GetQuestText(quest);
		});
	}

	private string GetQuestText(QuestObejctive quest) {
		sb.Clear();
		sb.Append(quest.ObjectiveText);
		if (quest.RequiredCount > 1) {
			sb.Append($" ({quest.CurrentCount} / {quest.RequiredCount})");
		}
		return sb.ToString();
	}

	private void HandleNewQuest(QuestObejctive quest) {
		if (Text != "") {
			PushSequence(ClearQuestText());
			Sequence seq = DOTween.Sequence();
			seq.AppendInterval(questChangeDelay);
			PushSequence(seq);
		}


		if (quest != null) {
			QuestLog($"Quest Controller: New Quest - {quest.name}");
			PushSequence(WriteText(GetQuestText(quest)));
		}
		else {
			QuestLog($"Quest Controller: New Quest - Nothing");
		}
	}


	[SerializeField] private bool debugLogQuestChanges;
	private void QuestLog(string s) {
		if (!debugLogQuestChanges) return;
		Debug.Log(s);
	}

	private void PushSequence(Sequence s) {
		sequences += s;
	}

	public void Progress() {
		questChain.currentObjetive.Progress();
	}

	public Sequence WriteText(string s) {
		Sequence seq = DOTween.Sequence();
		seq.AppendCallback(() => ResetQuestTextObject());
		var to = DOTween.To(() => Text, x => Text = x, s, s.Length * characterWriteDuration).SetOptions(true, ScrambleMode.None).SetEase(Ease.Linear);
		seq.Append(to);
		return seq;
	}

	public Sequence ChangeText(string s) {
		Sequence seq = DOTween.Sequence();
		seq.AppendCallback(() => Text = s);
		return seq;
	}

	public Sequence ClearQuestText() {
		Sequence seq = DOTween.Sequence();
		seq.Append(objectiveText.rectTransform.DOLocalMoveX(objTextLocalX + textMoveOffsetX, textMoveDuration).SetEase(Ease.InSine));
		Sequence seq2 = DOTween.Sequence();
		seq2.Append(DOTween.To(
			() => objectiveText.color.a, 
			(a) => {
				var color = objectiveText.color;
				color.a = a;
				objectiveText.color = color;
			},
			0, textMoveDuration).SetEase(Ease.InSine));
		seq2.Pause();
		seq.onPlay += () => {
			seq2.Play();
		};
		return seq;
	}

	public void ResetQuestTextObject() {
		var pos = objectiveText.rectTransform.localPosition;
		pos.x = objTextLocalX;
		objectiveText.rectTransform.localPosition = pos;

		var c = objectiveText.color;
		c.a = 1;
		objectiveText.color = c;

		Text = "";
		prevtexlen = 0;
		summedWriteTextSoundFrequency = 0;
	}

	public void StartTracking() {
		questChain.StartTracking();
	}

	private void Update() {
		if (Text.Length > prevtexlen) {
			prevtexlen = Text.Length;
			PlaySound();
		}
	}

	private void PlaySound() {
		summedWriteTextSoundFrequency += writeTextSoundFrequency;
		if (summedWriteTextSoundFrequency >= 1) {
			summedWriteTextSoundFrequency -= 1;
			writeTextSound.Play(src);
		}
	}
}
