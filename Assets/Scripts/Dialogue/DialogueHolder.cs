using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
#endif

[CreateAssetMenu (fileName = "DialogueHolder", menuName = "ScriptableObjects/Dialogue/DialogueHolder")]
public class DialogueHolder : ScriptableObject
{
    public DialoguePart[] parts;


#if UNITY_EDITOR
    [CustomEditor(typeof(DialogueHolder))]
    public class LookAtPointEditor : Editor {
        SerializedProperty parts;

        void OnEnable() {
            parts = serializedObject.FindProperty("parts");
        }

        public override void OnInspectorGUI() {
            serializedObject.Update();
            DrawDefaultInspector();

            if (GUILayout.Button("CreateNew")) {
                DialoguePart asset = ScriptableObject.CreateInstance<DialoguePart>();
                int index = parts.arraySize;
                parts.InsertArrayElementAtIndex(index);
                parts.GetArrayElementAtIndex(index).objectReferenceValue = asset;
                string path = AssetDatabase.GetAssetPath(this.target).Split('.')[0];
                string assetname = AssetDatabase.GetAssetPath(this.target).Split('/').Last().Split('.')[0];
                System.IO.Directory.CreateDirectory(Application.dataPath + path.Substring(6));
                AssetDatabase.CreateAsset(asset, path + "/" + assetname + " - " + parts.arraySize + " - " + Random.Range(0, 10000) + ".asset");
                AssetDatabase.SaveAssets();
                serializedObject.ApplyModifiedProperties();
            }
            int count = parts.arraySize;
            for (int i = 0; i < count; ++i) {
                var elem = parts.GetArrayElementAtIndex(i).objectReferenceValue;
                if (elem == null) continue;
                Editor e = Editor.CreateEditor(elem);
                e.OnInspectorGUI();
                //EditorGUILayout.PropertyField(parts.GetArrayElementAtIndex(i));
            }
            serializedObject.ApplyModifiedProperties();

        }
    }
#endif
}
