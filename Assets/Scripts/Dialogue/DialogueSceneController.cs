using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class DialogueSceneController : MonoBehaviour, DialogueInput {
	public static DialogueSceneController Instance { get; private set; }

	[SerializeField] private SoundMap SndShowTextbox;
	[SerializeField] private SoundMap SndHideTextbox;

	private AudioBuffer buffer;

	[Header("Components")]
	[SerializeField] private Camera cam;

	[SerializeField] private Transform actorPlatformLeft;
	[SerializeField] private Transform actorPlatformRight;

	[SerializeField] private RectTransform messageBox;
	[SerializeField] private TextMeshProUGUI nameField;
	[SerializeField] private TextMeshProUGUI textField;

	[Header("Offsets")]
	[SerializeField] private Vector3 actorOutsideRight;
	[SerializeField] private Vector3 actorInsideRight;
	[SerializeField] private Vector3 actorTalkingRight;

	[SerializeField] private float messageBoxYVisible = 50;
	[SerializeField] private float messageBoxYHidden;

	[Header("Timings")]
	[SerializeField] private float actorTransitionDuration = 0.6f;
	[SerializeField] private float msgBoxTransitionDuration = 0.25f;

	[SerializeField] private UpdateType dotweenUpdateType = UpdateType.Normal;


	private ActorPosition positionLeft = ActorPosition.Outside;
	private ActorPosition positionRight = ActorPosition.Outside;


	private string actorRight = null;
	private string actorLeft = null;

	private bool isPlaying = false;
	private bool canAdvance = false;

	private DialogueHolder currentDialogue;
	private int currentPart;
	private int currentSentence;

	private enum ActorPosition {
		Outside,
		Inside,
		Talking
	}

	[SerializeField] private GameObject gameUi;
	[SerializeField] private GameObject blackBars;
	[SerializeField] private float defaultBarsAnimationDuration;
	[SerializeField] private float barsContainerScaleHidden = 1.4f;
	[SerializeField] private float barsContainerScaleShown = 1.0f;

	public GameObject GameUI => gameUi;
	public GameObject BlackBars => blackBars;

	public Sequence ShowBlackBars() => ShowBlackBars(defaultBarsAnimationDuration);
	public Sequence ShowBlackBars(float duration) {
		if (duration < 0) duration = defaultBarsAnimationDuration;
		return DOTween.Sequence().AppendCallback(() => VisibleActions++).Append(blackBars.transform.DOScaleY(barsContainerScaleShown, duration).SetEase(Ease.OutSine));
	}

	public Sequence HideBlackBars() => HideBlackBars(defaultBarsAnimationDuration);
	public Sequence HideBlackBars(float duration) {
		if (duration < 0) duration = defaultBarsAnimationDuration;
		return DOTween.Sequence().Append(blackBars.transform.DOScaleY(barsContainerScaleHidden, duration).SetEase(Ease.InSine)).AppendCallback(() => VisibleActions--);
	}

	private int _visibleActions;
	private int VisibleActions {
		get => _visibleActions; set {
			_visibleActions = value;
			if (_visibleActions > 0) {
				cam.gameObject.SetActive(true);
			}
			else {
				cam.gameObject.SetActive(false);
			}
		}
	}


	private void Awake() {
		actorPlatformLeft.transform.localPosition = -actorOutsideRight;
		actorPlatformRight.transform.localPosition = actorOutsideRight;
		Instance = this;
		InputManager.Instance.dialogueInput = this;

		buffer = new AudioBuffer(2, gameObject);
	}

	private void OnDestroy() {
		if (Instance == this) Instance = null;
	}



	public void NextSentence(InputAction.CallbackContext x) {
		if (!x.started) return;
		if (!canAdvance) return;

		OnAdvance?.Invoke();
	}
	private delegate void ev();
	private event ev OnAdvance;

	private class DialogueId {
		public Sequence seq;
		public DialogueSceneController controller;

		public void Handle() {
			controller.DisplayNextSentence(this);
		}

		public void Register() {
			controller.OnAdvance += Handle;
		}

		public void Unregister() {
			controller.OnAdvance -= Handle;
		}
	}

	public void PlayDialogue(DialogueHolder dialogue) {
		if (isPlaying) {
			Debug.LogError("Trying to play dialogue while other dialogue is playing");
			return;
		}
		CreateDialogueSequence(dialogue).Play();
	}

	public Sequence CreateDialogueSequence(DialogueHolder dialogue) {
		//if (isPlaying) {
		//	Debug.LogError("Trying to play dialogue while other dialogue is playing");
		//	return DOTween.Sequence();
		//}

		bool initialized = false;

		Sequence sequcence = DOTween.Sequence();

		var diaId = new DialogueId();
		diaId.seq = sequcence;
		diaId.controller = this;

		sequcence.AppendCallback(() => {
			if (initialized) return;
			initialized = true;
			InputManager.Instance.EnableDialogueControl();

			VisibleActions++;

			currentDialogue = dialogue;
			currentPart = 0;
			currentSentence = 0;

			diaId.Register();

			if (isPlaying) {
				Debug.LogError("Trying to play dialogue while other dialogue is playing");
			}

			isPlaying = true;

			StartCoroutine(SetupNextPart());
			sequcence.Pause();
			sequcence.Rewind();
		});
		sequcence.AppendInterval(8f);
		sequcence.Pause();

		return sequcence;
	}

	private IEnumerator SetupNextPart() {
		Sequence block = null;
		var part = currentDialogue.parts[currentPart];

		// Move out old Actors if needed
		if (actorLeft != null) {
			if (part.actorLeft == null || actorLeft != part.actorLeft.name) {
				block = MoveActorLeft(ActorPosition.Outside);
			}
		}

		if (actorRight != null) {
			if (part.actorRight == null || actorRight != part.actorRight.name) {
				block = MoveActorRight(ActorPosition.Outside);
			}
		}

		if (block != null) yield return block.WaitForCompletion();
		block = null;

		// Switch Actor Models
		if (SwitchActorObject(actorPlatformLeft, part.actorLeft)) {
			actorLeft = part.actorLeft.name;
		}
		else {
			actorLeft = null;
		}
		if (SwitchActorObject(actorPlatformRight, part.actorRight)) {
			actorRight = part.actorRight.name;
		}
		else {
			actorRight = null;
		}

		// Move in Actors
		if (actorLeft != null) {
			block = MoveActorLeft(part.position == Position.Left ? ActorPosition.Talking : ActorPosition.Inside);
		}
		else {
			if (part.position == Position.Left && positionLeft == ActorPosition.Inside) {
				block = MoveActorLeft(ActorPosition.Talking);
			}
			else if (part.position != Position.Left && positionLeft == ActorPosition.Talking) {
				block = MoveActorLeft(ActorPosition.Inside);
			}
		}

		if (actorInsideRight != null) {
			block = MoveActorRight(part.position == Position.Right ? ActorPosition.Talking : ActorPosition.Inside);
		}
		else {
			if (part.position == Position.Right && positionRight == ActorPosition.Inside) {
				block = MoveActorRight(ActorPosition.Talking);
			}
			else if (part.position != Position.Right && positionRight == ActorPosition.Talking) {
				block = MoveActorRight(ActorPosition.Inside);
			}
		}

		if (block != null) yield return block.WaitForCompletion();
		block = null;

		// Text Handling

		textField.text = part.sentences[0];
		nameField.text = part.characterName;
		currentSentence = 0;

		yield return ShowTextbox().WaitForCompletion();

		canAdvance = true;
	}

	private void DisplayNextSentence(DialogueId id) {
		currentSentence++;
		var part = currentDialogue.parts[currentPart];
		
		if (currentSentence < part.sentences.Length) {
			textField.text = part.sentences[currentSentence];
			canAdvance = true;
			return;
		}

		canAdvance = false;

		StartCoroutine(EndPart(id));
	}

	private IEnumerator EndPart(DialogueId id) {
		yield return HideTextbox().WaitForCompletion();

		currentPart++;

		if (currentPart < currentDialogue.parts.Length) {
			yield return SetupNextPart();
		}
		else {
			yield return FinishUp(id);
		}
	}

	private IEnumerator FinishUp(DialogueId id) {
		YieldInstruction block = null;
		if (actorLeft != null && positionLeft != ActorPosition.Outside) {
			block = MoveActorLeft(ActorPosition.Outside).WaitForCompletion();
		}
		if (actorRight != null && positionRight != ActorPosition.Outside) {
			block = MoveActorRight(ActorPosition.Outside).WaitForCompletion();
		}

		if (block != null) yield return block;

		isPlaying = false;
		currentDialogue = null;
		SwitchActorObject(actorPlatformLeft, null);
		SwitchActorObject(actorPlatformRight, null);

		actorPlatformLeft.transform.localPosition = -actorOutsideRight;
		actorPlatformRight.transform.localPosition = actorOutsideRight;

		VisibleActions--;
		//InputManager.Instance.EnablePlayerControl();

		id.Unregister();
		id.seq.Play();
		id.seq.Goto(10);
	}



	private Sequence ShowTextbox() {
		Sequence seq = DOTween.Sequence();
		seq.SetUpdate(dotweenUpdateType);
		seq.AppendCallback(() => SndShowTextbox.Play(buffer.Get()));
		seq.Append(messageBox.DOLocalMoveY(messageBoxYVisible, msgBoxTransitionDuration).SetEase(Ease.OutSine));
		return seq;
	}

	private Sequence HideTextbox() {
		Sequence seq = DOTween.Sequence();
		seq.SetUpdate(dotweenUpdateType);
		seq.AppendCallback(() => SndHideTextbox.Play(buffer.Get()));
		seq.Append(messageBox.DOLocalMoveY(messageBoxYHidden, msgBoxTransitionDuration).SetEase(Ease.InSine));
		return seq;
	}

	private bool SwitchActorObject(Transform target, GameObject actorPrefab) {
		bool hasPrefab = actorPrefab != null;
		bool hasChild = target.childCount > 0;

		if (hasChild) {
			if (hasPrefab && actorPrefab.name == target.GetChild(0).gameObject.name) {
				return true;
			}
			else {
				Destroy(target.GetChild(0).gameObject);
			}
		}
		if (!hasPrefab) return false;

		Instantiate(actorPrefab, target).name = actorPrefab.name;

		return true;
	}

	private Sequence MoveActorRight(ActorPosition newPosition) {
		Sequence seq = null;
		switch (newPosition) {
			case ActorPosition.Outside:
				seq = MoveActor(actorOutsideRight, actorPlatformRight).SetEase(Ease.InSine);
				break;
			case ActorPosition.Inside:
				seq = MoveActor(actorInsideRight, actorPlatformRight);
				break;
			case ActorPosition.Talking:
				seq = MoveActor(actorTalkingRight, actorPlatformRight);
				break;
		}

		if (newPosition != ActorPosition.Outside && positionLeft == ActorPosition.Outside) {
			seq = seq.SetEase(Ease.OutSine);
		}
;
		positionRight = newPosition;

		return seq;
	}

	private Sequence MoveActorLeft(ActorPosition newPosition) {
		Sequence seq = null;
		switch (newPosition) {
			case ActorPosition.Outside:
				seq = MoveActor(-actorOutsideRight, actorPlatformLeft).SetEase(Ease.InSine);
				break;
			case ActorPosition.Inside:
				seq = MoveActor(-actorInsideRight, actorPlatformLeft);
				break;
			case ActorPosition.Talking:
				seq = MoveActor(-actorTalkingRight, actorPlatformLeft);
				break;
		}

		if (newPosition != ActorPosition.Outside && positionLeft == ActorPosition.Outside) {
			seq = seq.SetEase(Ease.OutSine);
		}

		positionLeft = newPosition;

		return seq;
	}

	private Sequence MoveActor(in Vector3 target, Transform transform) {
		Sequence seq = DOTween.Sequence();
		seq.SetUpdate(dotweenUpdateType);
		seq.Append(transform.DOLocalMove(target, actorTransitionDuration)).SetEase(Ease.InOutSine);
		return seq;
	}
}
