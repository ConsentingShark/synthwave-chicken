using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup), typeof(Canvas))]
public class LoadingScreen : MonoBehaviour, ILoadingScreen {
	[SerializeField] private float fadeDuration;
	private CanvasGroup group;
	private Canvas canvas;

	private void Start() {
		if (LevelController.Instance.loadingScreen != null) return;
		LevelController.Instance.loadingScreen = this;

		group = GetComponent<CanvasGroup>();
		canvas = GetComponent<Canvas>();
	}

	IEnumerator ILoadingScreen.Hide() {
		yield return group.DOFade(0, fadeDuration).SetUpdate(isIndependentUpdate: true).WaitForCompletion();
		canvas.enabled = false;
	}

	IEnumerator ILoadingScreen.Show() {
		canvas.enabled = true;
		yield return group.DOFade(1, fadeDuration).SetUpdate(isIndependentUpdate: true).WaitForCompletion();
	}
}
