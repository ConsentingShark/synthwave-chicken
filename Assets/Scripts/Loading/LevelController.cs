using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public interface ILoadingScreen {
	IEnumerator Show();
	IEnumerator Hide();
}

public class LevelController : MonoBehaviour {
	public static LevelController Instance { get; private set; }
	private static bool present = false;

	private bool loadInProgress;
	private List<AsyncOperation> loads = new List<AsyncOperation>(2);
	private List<AsyncOperation> unloads = new List<AsyncOperation>(2);

	[SerializeField] private int idleFramesBeforeHidingLoadingScreen;

	public ILoadingScreen loadingScreen;

	private void Awake() {
		if (Instance == null) {
			Instance = this;
			present = true;
			transform.parent = null;
			DontDestroyOnLoad(gameObject);
		}
		else {
			Destroy(this);
		}
	}

	private void _LoadLevel(int buildIndex) {
		StartCoroutine(HandleLoadLevel(buildIndex));
	}

	public static void LoadLevel(int buildIndex) {
		if (!present) {
			Debug.LogError("No LevelController present. Falling back to regular scene loading");
			SceneManager.LoadScene(buildIndex, LoadSceneMode.Single);
			return;
		}

		Instance._LoadLevel(buildIndex);
	}

	private IEnumerator HandleLoadLevel(params int[] indiciesToLoad) {
		if (loadInProgress) yield break;

		loadInProgress = true;
		loads.Clear();
		unloads.Clear();

		yield return loadingScreen.Show();

		Time.timeScale = 0;

		int sceneCount = SceneManager.sceneCount;
		for (int i = sceneCount -1; i >= 0; --i) {
			var scene = SceneManager.GetSceneAt(i);
			unloads.Add(SceneManager.UnloadSceneAsync(scene.buildIndex));
		}

		foreach(var unload in unloads) {
			yield return unload;
		}

		for (int i = 0; i < indiciesToLoad.Length; ++i) {
			var op = SceneManager.LoadSceneAsync(indiciesToLoad[i], i > 0 ? LoadSceneMode.Additive : LoadSceneMode.Single);
			op.allowSceneActivation = false;
			loads.Add(op);
		}

		foreach (var load in loads) {
			while (load.progress < 0.9f) {
				yield return null;
			}
		}

		foreach (var load in loads) {
			load.allowSceneActivation = true;
		}

		foreach (var load in loads) {
			yield return load;
		}

		Time.timeScale = 1;

		for (int _ = 0; _ < idleFramesBeforeHidingLoadingScreen; ++_) {
			yield return null;
		}

		//Time.timeScale = 1;
		yield return loadingScreen.Hide();

		loadInProgress = false;
	}
}
