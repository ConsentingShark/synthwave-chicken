using System;
using System.Collections;
using System.Collections.Generic;
using Data;
using TMPro;
using UnityEngine;
using Random = System.Random;

public class CreditsPlayer : MonoBehaviour
{
    [SerializeField] public float creditsCooldown = 2f;
    [SerializeField] public HealthComponent healthComponent;
    [SerializeField] public List<CreditsData> creditsDatas;
    [SerializeField] public TextMeshProUGUI text;

    private static bool _isCurrentlyShowingCredits = false;
    private static bool _showedEveryOneOnce = false;
    private static int _counter = 0;
    private static Random _random = new Random();

    public static void ResetCredits()
    {
        _isCurrentlyShowingCredits = false;
        _showedEveryOneOnce = false;
        _counter = 0;
    }
    
    private void OnEnable()
    {
        healthComponent.OnDamage += OnDamage;
    }

    private void OnDisable()
    {
        healthComponent.OnDamage -= OnDamage;
    }

    
    
    private void OnDamage(int damage, bool died, DamageType _ = DamageType.Unset)
    {
        if (!_isCurrentlyShowingCredits)
        {
            _isCurrentlyShowingCredits = true;

            if (!_showedEveryOneOnce)
            {
                ShowCredits(creditsDatas[_counter]);
                _counter++;

                if (_counter >= creditsDatas.Count) _showedEveryOneOnce = true;
            }
            else
            {
                ShowCredits(creditsDatas[_random.Next(creditsDatas.Count)]);
            }
            
            StartCoroutine(CooldownCredits());
        }   
    }

    private void ShowCredits(CreditsData creditsData)
    {
        String joke = "";
        if (creditsData.sideJoke.Count > 0)
        {
            joke = creditsData.sideJoke[_random.Next(creditsData.sideJoke.Count)];
        }
        
        text.text = $"{creditsData.name}: {creditsData.department}\n\n{joke}";
    }

    private IEnumerator CooldownCredits()
    {
        yield return new WaitForSeconds(creditsCooldown);
        _isCurrentlyShowingCredits = false;
    }
}
