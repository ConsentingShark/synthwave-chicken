using Data;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] private PlaySettings playSettings;
        
        public void StartGame()
        {
            playSettings.isStoryPlaythrough = true;
            playSettings.currentLevel = playSettings.allLevel.LevelPreviews[0];
            playSettings.currentTime = 0f;
            LevelController.LoadLevel(playSettings.currentLevel.LevelNumber);
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}
