using System;
using System.Collections;
using System.Collections.Generic;
using Data;
using TMPro;
using UnityEditor;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI timerText;
    private float _time = 0f;
    private bool _isTimerRunning = true;

    [SerializeField] private string prefix = "";
    [SerializeField] private string sufix = "";
    [SerializeField] private PlaySettings playSettings;
    [SerializeField] private bool runAtLevelStart = true;
    
    // Start is called before the first frame update
    void Start()
    {
        timerText.text = prefix + "00:00:00.000" + sufix;
        
        _time = (!playSettings.isStoryPlaythrough || playSettings.currentLevel == playSettings.allLevel.LevelPreviews[0]) ? 0f : playSettings.currentTime;

        if (runAtLevelStart)
        {
            StartTimer();
        }
        else
        {
            PauseTimer();
        }
    }

    private void Update()
    {
        if (_isTimerRunning)
        {
            _time += Time.deltaTime;
            playSettings.currentTime = _time;
        }
        TimeSpan ts = TimeSpan.FromSeconds(_time);
        timerText.text = prefix + ts.ToString(@"hh\:mm\:ss\.fff") + sufix;
    }

    public void ResetTimer()
    {
        _time = 0f;
        playSettings.currentTime = _time;
    }

    public void StartTimer()
    {
        _isTimerRunning = true;
    }

    public void PauseTimer()
    {
        _isTimerRunning = false;
    }
}
