using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Data;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.UI;

public class YouWon : MonoBehaviour
{
    [SerializeField] private GameObject backToMenu;
    [SerializeField] private GameObject finishedText;
    [SerializeField] private GameObject youWonImage;
    [SerializeField] private PlaySettings playSettings;
    
    // Start is called before the first frame update
    void Start()
    {
        if (playSettings.currentLevel.isBossLevel)
        {
            if (playSettings.isStoryPlaythrough)
            {
                backToMenu.SetActive(false);
            }
            else
            {
                backToMenu.SetActive(true);
            }
            youWonImage.SetActive(true);
            finishedText.SetActive(false);
        }
        else
        {
            backToMenu.SetActive(true);
            finishedText.SetActive(true);
            youWonImage.SetActive(false);
        }
    }
}
