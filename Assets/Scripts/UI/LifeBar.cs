using System;
using System.Collections;
using System.Globalization;
using Data;
using Data.Player;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class LifeBar : MonoBehaviour
    {
        [SerializeField] private HealthCounter healthCounter;
        [SerializeField] private AnimationCurve healthLie;
        [SerializeField] private Image healthIndicator;
        [SerializeField] private float lerpBlendTime;

        private float currentFill;

        private void SetLife(int health) {
            float fill = (float)health / healthCounter.settings.maxHealth;
            fill = healthLie.Evaluate(fill);
            currentFill = Mathf.Lerp(currentFill, fill, Time.deltaTime * lerpBlendTime);
            healthIndicator.material.SetFloat("_Fill", currentFill);
        }

        private void Start()
        {
            healthIndicator.material = new Material(healthIndicator.material);
            currentFill = healthCounter.health / (float)healthCounter.settings.maxHealth;
            SetLife(healthCounter.health);
        }

        private void Update()
        {
            SetLife(healthCounter.health);
        }
    }
}