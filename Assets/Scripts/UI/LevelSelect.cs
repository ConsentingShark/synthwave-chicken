using System;
using System.Collections;
using System.Collections.Generic;
using Data;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelect : MonoBehaviour
{
    [SerializeField] private Image levelPreview;
    [SerializeField] private TextMeshProUGUI textMesh;
    [SerializeField] private PlaySettings playSettings;
    private int _currentlySelected = 0;

    public void Start()
    {
        textMesh.text = "";
        _currentlySelected = 0;
        LoadPreview();
    }

    public void NextLevel()
    {
        do
        {
            _currentlySelected++;
            _currentlySelected %= playSettings.allLevel.LevelPreviews.Count;
        } while (!playSettings.allLevel.LevelPreviews[_currentlySelected].isSelectable && _currentlySelected != 0);
        
        LoadPreview();
    }

    public void PrevLevel()
    {
        do
        {
            _currentlySelected--;
            _currentlySelected += playSettings.allLevel.LevelPreviews.Count;
            _currentlySelected %= playSettings.allLevel.LevelPreviews.Count;
        } while (!playSettings.allLevel.LevelPreviews[_currentlySelected].isSelectable && _currentlySelected != 0);

        LoadPreview();
    }

    public void StartLevel()
    {
        Debug.Log("PlaySettings");
        playSettings.isStoryPlaythrough = false;
        playSettings.currentLevel = playSettings.allLevel.LevelPreviews[_currentlySelected];

        LevelController.LoadLevel(playSettings.allLevel.LevelPreviews[_currentlySelected].LevelNumber);
    }

    private void LoadPreview()
    {
        if (playSettings.allLevel.LevelPreviews.Count >= 0)
        {
            levelPreview.sprite = playSettings.allLevel.LevelPreviews[_currentlySelected].Preview;
            textMesh.text = playSettings.allLevel.LevelPreviews[_currentlySelected].Name;
        }
    }
}