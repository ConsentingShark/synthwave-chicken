using DG.Tweening;
using Enemies;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;
using UnityEngine.Serialization;
using Cinemachine;


#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.UIElements;
#endif


public enum CutsceneSequenceType {
	/*
	 * Params: value, vector.x
	 * Optional vector.y post delay after fade
	 * Optional vector.z pre delay before fade
	 * Parallel if value == -1
	 * Duration = vector.x if vector.x > 0, GameObject configured value else
	 */
	FadeIn = 0,
	FadeOut = 1,

	/*
	 * Params: dialogue, vector.x, vector.y
	 * dialogue gets played
	 * vector x : delay before
	 * vector y : delay after
	 */
	Dialogue = 2,

	/*
	 * Params: QuestObjective
	 * QuestObjective gets progress
	 */
	Quest = 3,

	/*
	 * Params: event
	 * event gets invoked
	 */
	Event = 4,

	/*
	 * Params: value
	 * Value is used as duration of delay
	 */
	Delay = 5,

	/*
	 * Params: cam, value
	 * cam: target cimemachine virtual camera, playercam if null
	 * wait for completion if value == 1
	 */
	MoveCamera = 6,

	/*
	 * Params: vector, transform, value
	 * Rotates chicken towards transform if set, otherwise vector is used as direction value
	 * Rotation is instant if value == 1
	 */
	ChickenLookAt = 7,

	/*
	 * Params: transform, value
	 * Chicken walks close to transform with value [0,1] percentage of speed
	 */
	ChickenMoveTo = 8,

	/*
	 * Params: transform
	 */
	ChickenTeleport = 9,

	/*
	 * Params: value, vector.x, vector.y
	 * Shows if value == 1, hides else
	 * vector.x = duration of animation
	 * parallel if vector.y == 1
	 */
	CutsceneUI = 20,
	GameUI = 21
}

[System.Serializable]
public struct CutsceneSequence {
	public CutsceneSequenceType type;
	public DialogueHolder dialogue;
	public QuestObejctive objective;
	public Transform transform;
	public float value;
	public Vector4 vector;
	public CinemachineVirtualCamera cam;
	public UnityEvent @event;
}

public class Cutscene : MonoBehaviour {
	[SerializeField] private CutsceneSequence[] sequences;

	[FormerlySerializedAs("blendDuration")]
	[SerializeField] private float defaultFadeDuration = 0.4f;

	[SerializeField] private float moveAccuracy = 0.25f;
	[SerializeField] private float moveCloseEnough = 0.1f;

	[SerializeField] private bool autoHideGameUi = false;
	[SerializeField] private bool autoShowBlackBars = false;
	[SerializeField] private bool DisableControl = true;

	[SerializeField] private GlobalEnemySettings enemySettings;
	[SerializeField] private UnityEvent OnCutsceneComplete;

	private SequenceQueue queue;
	private ChickenController chicken;

	private CinemachineVirtualCamera currentCMCam;
	private float defaultCMCamBlendDuration;

	private void Start() {
		chicken = GameObject.FindGameObjectWithTag("Player").GetComponent<ChickenController>();
		defaultCMCamBlendDuration = Camera.main.GetComponent<CinemachineBrain>().m_DefaultBlend.BlendTime;
	}

	public void Play() {
		queue = new SequenceQueue();
		InputManager.Instance.EnableDialogueControl();
		CreateQueue();
	}

	private static Sequence StallingSequence(Action<Action> OnStart) {
		Sequence sequence = DOTween.Sequence();

		Action resume = () => {
			sequence.Goto(11, true);
		};

		sequence.AppendCallback(() => {
			OnStart(resume);
			sequence.Pause();
		});
		sequence.AppendInterval(10);

		return sequence;
	}

	private void CreateQueue() {
		var (prepare, cleanup) = PrepareCutscene();
		queue += prepare;

		if (autoHideGameUi) CutsceneUI();
		if (autoShowBlackBars) queue += BlackBars(true);

		foreach (var sub in sequences) {
			switch (sub.type) {
				case CutsceneSequenceType.Dialogue:
					if (sub.vector.x > 0) queue += DOTween.Sequence().AppendInterval(sub.vector.x);
					queue += DialogueSceneController.Instance.CreateDialogueSequence(sub.dialogue);
					if (sub.vector.y > 0) queue += DOTween.Sequence().AppendInterval(sub.vector.y);
					break;

				case CutsceneSequenceType.Quest:
					queue += DOTween.Sequence().AppendCallback(() => sub.objective.Progress());
					break;

				case CutsceneSequenceType.Event:
					queue += DOTween.Sequence().AppendCallback(() => sub.@event.Invoke());
					break;

				case CutsceneSequenceType.Delay:
					queue += DOTween.Sequence().AppendInterval(sub.value);
					break;

				case CutsceneSequenceType.FadeIn:
					queue += Fade(sub, true);
					break;

				case CutsceneSequenceType.FadeOut:
					queue += Fade(sub, false);
					break;

				case CutsceneSequenceType.MoveCamera:
					queue += MoveCamera(sub);
					break;

				case CutsceneSequenceType.ChickenLookAt:
					queue += DOTween.Sequence().AppendCallback(() => ChickenLookAt(sub));
					break;

				case CutsceneSequenceType.ChickenMoveTo:
					queue += StallingSequence((resume) => StartCoroutine(MoveTo(sub, resume)));
					break;

				case CutsceneSequenceType.ChickenTeleport:
						queue += DOTween.Sequence().AppendCallback(() => chicken.transform.position = sub.transform.position);
						break;

				case CutsceneSequenceType.CutsceneUI:
					queue += BlackBars(sub);
					break;

				case CutsceneSequenceType.GameUI:
					GameUI();
					break;
			}
		}

		if (autoHideGameUi) GameUI();
		if (autoShowBlackBars) queue += BlackBars(false);

		queue += cleanup;

		queue += DOTween.Sequence().AppendCallback(() => OnCutsceneComplete.Invoke());
	}

	private void CutsceneUI() {
		queue += DOTween.Sequence().AppendCallback(() => {
			DialogueSceneController.Instance.BlackBars.SetActive(true);
			DialogueSceneController.Instance.GameUI.SetActive(false);
			});
	}

	private void GameUI() {
		queue += DOTween.Sequence().AppendCallback(() => {
			DialogueSceneController.Instance.BlackBars.SetActive(false);
			DialogueSceneController.Instance.GameUI.SetActive(true);
		});
	}

	private Sequence BlackBars(in CutsceneSequence sub) {
		return BlackBars(sub.value == 1, sub.vector.y, sub.vector.x == 1);
	}

	private Sequence BlackBars(bool show, float duration = -1, bool parallel = true) {
		Sequence seq =  show ? DialogueSceneController.Instance.ShowBlackBars(duration) : DialogueSceneController.Instance.HideBlackBars(duration);
		if (!parallel) return seq;

		seq.Pause();

		return DOTween.Sequence().AppendCallback(() => seq.Play());
	}

	private Sequence Fade(in CutsceneSequence sub, bool fadeIn) {
		float duration = sub.vector.x > 0 ? sub.vector.x : defaultFadeDuration;
		Sequence seq = fadeIn ? BlackBlend.Instance.FadeIn(duration) : BlackBlend.Instance.FadeOut(duration);
		if (sub.value == -1) {
			seq.Pause();
			return DOTween.Sequence().AppendCallback(() => seq.Play());
		}
		else {
			if (sub.vector.z > 0) {
				seq.PrependInterval(sub.vector.z);
			}
			if (sub.vector.y > 0) {
				seq.AppendInterval(sub.vector.y);
			}
		}
		return seq;
	}

	private void ChickenLookAt(in CutsceneSequence sub) {
		bool instant = sub.value == 1;
		if (sub.transform != null) {
			Vector3 vec = sub.transform.position - chicken.Model.transform.position;
			vec.y = 0;
			vec.Normalize();
			chicken.CutsceneOverrideViewDirection(vec, instant);
		}
		else {
			Vector3 vec = sub.vector;
			vec.y = 0;
			vec.Normalize();
			chicken.CutsceneOverrideViewDirection(vec, instant);
		}
	}

	private IEnumerator MoveTo(CutsceneSequence sub, Action resumeCutscene) {
		bool done = false;
		while (true) {
			Vector3 chickenPosition = chicken.Model.transform.position;
			chickenPosition.y = 0;

			Vector3 targetPosition = sub.transform.position;
			targetPosition.y = 0;

			// Distance Check
			float distance = Vector3.Distance(chickenPosition, targetPosition);

			if (done ||distance < moveCloseEnough) {
				if (!done) {
				chicken.CutsceneOverrideMoveVelocity(Vector2.zero);
				}
				done = true;

				if (!chicken.Moving) {
					resumeCutscene();
					yield break;
				}

				yield return null;
				continue;
			}

			// Rotate
			Vector3 direction = targetPosition - chickenPosition;
			direction.y = 0;
			direction.Normalize();
			chicken.CutsceneOverrideViewDirection(direction, false);

			// Move;
			float velocityFactor = Mathf.Clamp01(distance / moveAccuracy);

			Vector2 velocity = new Vector2(direction.x, direction.z);
			velocity *= (velocityFactor * sub.value);

			chicken.CutsceneOverrideMoveVelocity(velocity);
			yield return null;
		}
	}

	private (Sequence, Sequence) PrepareCutscene() {
		var prepare = DOTween.Sequence().AppendCallback(() => {
			if (enemySettings != null) enemySettings.isAIActive = false;
			else Debug.LogWarning($"Cutscene in {gameObject.name} has no Enemy Settings!");
			if (DisableControl) InputManager.Instance.EnableDialogueControl();
		});
		var cleanup = DOTween.Sequence().AppendCallback(() => {
			if (enemySettings != null) enemySettings.isAIActive = true;
			if (DisableControl) InputManager.Instance.EnablePlayerControl();
		});

		return (prepare, cleanup);
	}

	private Sequence MoveCamera(in CutsceneSequence sub) {
		var cam = sub.cam;
		Sequence seq = DOTween.Sequence().AppendCallback(() => {
			if (currentCMCam != null) currentCMCam.Priority = 0;
			if (cam != null) {
				currentCMCam = cam;
				currentCMCam.Priority = 999;
			}
		});
		if (sub.value == 1) {
			seq.AppendInterval(defaultCMCamBlendDuration);
		}
		return seq;
	}
}


#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(CutsceneSequence))]
[CanEditMultipleObjects]
public class CutsceneSequenceDrawerUIE : PropertyDrawer {
	private float lineSpacing => EditorGUIUtility.standardVerticalSpacing;

	private SerializedProperty type;
	private SerializedProperty dialogue;
	private SerializedProperty objective;
	private SerializedProperty transform;
	private SerializedProperty value;
	private SerializedProperty vector;
	private SerializedProperty cam;
	private SerializedProperty @event;

	private string name;
	private bool cache = false;

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		void LineBreak(ref Rect position) {
			position.y += EditorGUIUtility.singleLineHeight + lineSpacing;
		}

		if (!cache || name != property.displayName) {
			name = property.displayName;

			type = property.FindPropertyRelative("type");
			dialogue = property.FindPropertyRelative("dialogue");
			objective = property.FindPropertyRelative("objective");
			transform = property.FindPropertyRelative("transform");
			value = property.FindPropertyRelative("value");
			vector = property.FindPropertyRelative("vector");
			cam = property.FindPropertyRelative("cam");
			@event = property.FindPropertyRelative("event");

			cache = true;
		}

		Vector4 vector4;
		Vector4 vector3;
		bool b;

		EditorGUI.BeginProperty(position, GUIContent.none, property);

		// GUI Implementation
		position.height = EditorGUIUtility.singleLineHeight;
		property.isExpanded = EditorGUI.Foldout(position, property.isExpanded, GUIContent.none);
		Rect contentPosition = EditorGUI.PrefixLabel(position, new GUIContent(name));
		EditorGUI.PropertyField(contentPosition, type, GUIContent.none);

		if (!property.isExpanded) {
			EditorGUI.EndProperty();
			return;
		}

		CutsceneSequenceType seqType = (CutsceneSequenceType)(Enum.GetValues(typeof(CutsceneSequenceType)).GetValue(type.enumValueIndex));

		//position.y += EditorGUIUtility.singleLineHeight + lineSpacing;
		LineBreak(ref position);

		EditorGUI.indentLevel++;

		switch (seqType) {
			case CutsceneSequenceType.FadeIn:
			case CutsceneSequenceType.FadeOut:
				b = value.floatValue == -1;
				EditorGUI.BeginChangeCheck();
				b = EditorGUI.Toggle(EditorGUI.IndentedRect(position), new GUIContent("Parallel"), b);
				if (EditorGUI.EndChangeCheck()) {
					value.floatValue = b ? -1 : 0;
				}

				//position.y += EditorGUIUtility.singleLineHeight + lineSpacing;
				LineBreak(ref position);


				vector4 = vector.vector4Value;
				EditorGUI.BeginChangeCheck();

				vector4.x = EditorGUI.FloatField(EditorGUI.IndentedRect(position), new GUIContent("Duration"), vector4.x);

				if (!b) {
					//position.y += EditorGUIUtility.singleLineHeight + lineSpacing;
					LineBreak(ref position);
					vector4.z = EditorGUI.FloatField(EditorGUI.IndentedRect(position), new GUIContent("Pre Delay"), vector4.z);

					//position.y += EditorGUIUtility.singleLineHeight + lineSpacing;
					LineBreak(ref position);
					vector4.y = EditorGUI.FloatField(EditorGUI.IndentedRect(position), new GUIContent("Post Delay"), vector4.y);
				}

				if (EditorGUI.EndChangeCheck()) {
					vector.vector4Value = vector4;
				}

				break;

			case CutsceneSequenceType.Dialogue:
				EditorGUI.PropertyField(EditorGUI.IndentedRect(position), dialogue, GUIContent.none);
				break;

			case CutsceneSequenceType.Quest:
				EditorGUI.PropertyField(EditorGUI.IndentedRect(position), objective, GUIContent.none);
				break;

			case CutsceneSequenceType.Event:
				EditorGUI.PropertyField(EditorGUI.IndentedRect(position), @event, GUIContent.none);
				break;

			case CutsceneSequenceType.Delay:
				EditorGUI.PropertyField(EditorGUI.IndentedRect(position), value, GUIContent.none);
				break;

			case CutsceneSequenceType.MoveCamera:
				EditorGUI.PropertyField(EditorGUI.IndentedRect(position), cam, GUIContent.none);

				LineBreak(ref position);

				b = value.floatValue == 1;
				EditorGUI.BeginChangeCheck();
				b = EditorGUI.Toggle(EditorGUI.IndentedRect(position), new GUIContent("Wait for finish"), b);
				if (EditorGUI.EndChangeCheck()) {
					value.floatValue = b ? 1 : 0;
				}
				break;

			case CutsceneSequenceType.ChickenLookAt:
				EditorGUI.PropertyField(EditorGUI.IndentedRect(position), transform);

				var transformVal = transform.objectReferenceValue;

				//position.y += EditorGUIUtility.singleLineHeight + lineSpacing;
				LineBreak(ref position);

				if (transformVal == null) {
					vector4 = vector.vector4Value;
					EditorGUI.BeginChangeCheck();
					vector3 = EditorGUI.Vector3Field(EditorGUI.IndentedRect(position), new GUIContent("Direction"), vector4);
					if (EditorGUI.EndChangeCheck()) {
						vector.vector4Value = new Vector4(vector3.x, vector3.y, vector3.z, vector4.w);
					}
				}
				else {
					EditorGUI.LabelField(EditorGUI.IndentedRect(position), "Direction overwritten by set Transform");
				}

				//position.y += EditorGUIUtility.singleLineHeight + lineSpacing;
				LineBreak(ref position);

				b = value.floatValue == 1;
				EditorGUI.BeginChangeCheck();
				b = EditorGUI.Toggle(EditorGUI.IndentedRect(position), new GUIContent("Instant"), b);
				if (EditorGUI.EndChangeCheck()) {
					value.floatValue = b ? 1 : 0;
				}

				break;

			case CutsceneSequenceType.ChickenMoveTo:
				EditorGUI.PropertyField(EditorGUI.IndentedRect(position), transform, new GUIContent("Target"));

				//position.y += EditorGUIUtility.singleLineHeight + lineSpacing;
				LineBreak(ref position);

				EditorGUI.PropertyField(EditorGUI.IndentedRect(position), value, new GUIContent("Velocity Scale"));

				break;

			case CutsceneSequenceType.ChickenTeleport:
				EditorGUI.PropertyField(EditorGUI.IndentedRect(position), transform, new GUIContent("Target"));
				break;

			case CutsceneSequenceType.CutsceneUI:
				b = value.floatValue == 1;
				EditorGUI.BeginChangeCheck();
				b = EditorGUI.Toggle(EditorGUI.IndentedRect(position), new GUIContent("Show"), b);
				if (EditorGUI.EndChangeCheck()) {
					value.floatValue = b ? 1 : 0;
				}

				LineBreak(ref position);

				vector4 = vector.vector4Value;
				EditorGUI.BeginChangeCheck();

				vector4.x = EditorGUI.FloatField(EditorGUI.IndentedRect(position), new GUIContent("Duration"), vector4.x);

				//position.y += EditorGUIUtility.singleLineHeight + lineSpacing;
				LineBreak(ref position);
				vector4.y = EditorGUI.Toggle(EditorGUI.IndentedRect(position), new GUIContent("Parallel"), vector4.y == 1) ? 1 : vector4.y;

				if (EditorGUI.EndChangeCheck()) {
					vector.vector4Value = vector4;
				}

				break;

			case CutsceneSequenceType.GameUI:
				break;

			default:
				break;
		}

		EditorGUI.indentLevel--;

		EditorGUI.EndProperty();
	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
		if (!property.isExpanded) return EditorGUIUtility.singleLineHeight;
		return GetPropertyHeightInner(property, label) + lineSpacing;
	}

	public float GetPropertyHeightInner(SerializedProperty property, GUIContent label) {
		CutsceneSequenceType seqType = (CutsceneSequenceType)(Enum.GetValues(typeof(CutsceneSequenceType)).GetValue(property.FindPropertyRelative("type").enumValueIndex));
		switch (seqType) {
			case CutsceneSequenceType.FadeIn:
			case CutsceneSequenceType.FadeOut:
				if (property.FindPropertyRelative("value").floatValue == -1 ) {
					return 3 * EditorGUIUtility.singleLineHeight + 2 * lineSpacing;
				}
				return 5 * EditorGUIUtility.singleLineHeight + 4 * lineSpacing;

			case CutsceneSequenceType.ChickenMoveTo:
				return 3 * EditorGUIUtility.singleLineHeight + 2 * lineSpacing;

			case CutsceneSequenceType.Quest:
			case CutsceneSequenceType.Dialogue:
			case CutsceneSequenceType.Delay:
			case CutsceneSequenceType.ChickenTeleport:
				return EditorGUIUtility.singleLineHeight * 2 + lineSpacing;

			case CutsceneSequenceType.MoveCamera:
				return 3 * EditorGUIUtility.singleLineHeight + 2 * lineSpacing;


			case CutsceneSequenceType.Event:
				return EditorGUIUtility.singleLineHeight + lineSpacing + EditorGUI.GetPropertyHeight(property.FindPropertyRelative("event"));

			case CutsceneSequenceType.CutsceneUI:
			case CutsceneSequenceType.ChickenLookAt:
				return 4 * EditorGUIUtility.singleLineHeight + 3 * lineSpacing;

			case CutsceneSequenceType.GameUI:
				break;
		}
		return EditorGUIUtility.singleLineHeight;
	}
}
#endif

