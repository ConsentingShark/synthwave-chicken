using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class HitColorDebugVis : MonoBehaviour {
	[SerializeField] private Renderer rdr;

	[SerializeField] private bool emit = false;
	[SerializeField] private float hdr = 4;

	[SerializeField] private Color red = Color.red;
	[SerializeField] private Color blue = Color.blue;
	[SerializeField] private Color green = Color.green;

	[SerializeField] private bool isStartingTriggered = false;

	private void OnEnable() {
		GetComponent<HealthComponent>().OnDamage += OnDamage;
		if (isStartingTriggered)
		{
			int j = Random.Range(1, 4);
			for (int i = 0; i < j; i++)
			{
				OnDamage(0,true);
			}
		}
	}

	private void OnDisable() {
		GetComponent<HealthComponent>().OnDamage -= OnDamage;
	}

	private void OnDamage(int a, bool b, DamageType _ = DamageType.Unset) {
		if (emit) {
			rdr.material.EnableKeyword("_EMISSION");
			rdr.material.color = Color.white;
			var cc = rdr.material.GetColor("_EmissionColor") / hdr;
			if (cc == red) cc = green;
			else if (cc == green) cc = blue;
			else cc = red;
			cc *= hdr;
			rdr.material.SetColor("_EmissionColor", cc);
		}
		else {
			if (rdr.material.color == Color.red) rdr.material.color = Color.green;
			else if (rdr.material.color == Color.green) rdr.material.color = Color.blue;
			else rdr.material.color = Color.red;
			rdr.material.SetColor("_EmissionColor", Color.black);
		}
	}
}
