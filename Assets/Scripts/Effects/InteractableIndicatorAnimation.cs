using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableIndicatorAnimation : MonoBehaviour {
	[SerializeField] private bool usePulse;
	[SerializeField] private MeshRenderer[] indicators;
	[ColorUsage(false, true)] [SerializeField] private Color light;
	[ColorUsage(false, true)] [SerializeField] private Color dark;
	[SerializeField] private float duration;

	[Space]
	[SerializeField] private bool useWobble;
	[SerializeField] private float wobbleDuration;
	[SerializeField] private Vector3 wobbleOffset;
	[SerializeField] private Transform wobbleTransform;

	[Space]
	[SerializeField] private bool useScaling;
	[SerializeField] private float scalinDurationDuration;
	[SerializeField] private Vector3 scalingOffset;
	[SerializeField] private Transform scalingTransform;

	[Space]
	[SerializeField] private bool useRotation;
	[SerializeField] private float rotationDuration;
	[SerializeField] private Transform rotationTransform;

	[Space]
	[SerializeField] private Transform[] collapseScalingContainers;
	[SerializeField] private AnimationCurve collapseCurve;
	[SerializeField] private float collapseDuration;

	private bool disposed;
	private Sequence pulse;
	private Sequence wobble;
	private Sequence scaling;
	private Sequence rotation;

	private void Awake() {
		// Pulse
		if (usePulse) {
			Material materialPrefab = indicators[0].material;
			foreach (var renderer in indicators) {
				renderer.material = new Material(materialPrefab);
				renderer.material.SetColor("_EmissionColor", dark);
			}

			Sequence seq = DOTween.Sequence();

			foreach (var renderer in indicators) {
				seq.AppendCallback(() => renderer.material.SetColor("_EmissionColor", light));
				seq.AppendInterval(duration);
				seq.AppendCallback(() => renderer.material.SetColor("_EmissionColor", dark));
			}

			seq.SetLoops(-1);
			seq.Play();

			pulse = seq;
		}

		// Wobble
		if (useWobble) {
			wobbleTransform.localPosition = wobbleOffset;

			Sequence seq = DOTween.Sequence();
			seq.Append(wobbleTransform.DOLocalMove(-wobbleOffset, wobbleDuration).SetEase(Ease.InOutSine));
			seq.Append(wobbleTransform.DOLocalMove(wobbleOffset, wobbleDuration).SetEase(Ease.InOutSine));

			seq.SetLoops(-1);
			seq.Play();

			wobble = seq;
		}

		// Scaling
		if (useScaling) {
			Vector3 baseScale = scalingTransform.localScale;
			scalingTransform.localScale += scalingOffset;

			Sequence seq = DOTween.Sequence();
			seq.Append(scalingTransform.DOScale(baseScale - scalingOffset, scalinDurationDuration).SetEase(Ease.InOutSine));
			seq.Append(scalingTransform.DOScale(baseScale + scalingOffset, scalinDurationDuration).SetEase(Ease.InOutSine));

			seq.SetLoops(-1);
			seq.Play();

			scaling = seq;
		}

		// Rotation
		if (useRotation) {
			Sequence seq = DOTween.Sequence();
			seq.Append(rotationTransform.DOLocalRotate(new Vector3(0, 360, 0), rotationDuration, RotateMode.LocalAxisAdd).SetEase(Ease.Linear));
			seq.SetLoops(-1);
			seq.Play();
		}
	}

	public void Dispose() {
		if (disposed) return;
		disposed = true;

		if (usePulse) pulse.Pause();

		Sequence seq = DOTween.Sequence();
		seq.Append(DOTween.Sequence());

		foreach (var renderer in indicators) {
			seq.Join(DOTween.To(
				() => renderer.material.GetColor("_EmissionColor"),
				(v) => renderer.material.SetColor("_EmissionColor", v),
				new Color(0, 0, 0, 0),
				0.5f
				));

		}
		seq.AppendCallback(() => Destroy(gameObject));
		seq.Play();
	}

	public void Collaps() {
		if (disposed) return;
		disposed = true;

		if (usePulse) pulse.Pause();

		Sequence seq = DOTween.Sequence();
		foreach (var t in collapseScalingContainers) {
			seq.Join(t.DOScale(0, collapseDuration).SetEase(collapseCurve));
		}

		seq.AppendCallback(() => Destroy(gameObject));
		seq.Play();
	}

	private void OnDestroy() {
		if (useWobble) wobble.Kill();
		if (usePulse) pulse.Kill();
		if (useRotation) rotation.Kill();
		if (useScaling) scaling.Kill();
	}
}
