using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemParticleHelper : MonoBehaviour {
	private ParticleSystem system;
	private ParticleSystem.Particle[] particles;

	private void Awake() {
		system = GetComponent<ParticleSystem>();
	}

	public void Emitting(bool b) {
		var emission = system.emission;
		emission.enabled = b;
	}

	public void ExternalForce(bool b) {
		var external = system.externalForces;
		external.enabled = b;
	}

	public void SetRemainingLifetimeAll(float f) {
		if (particles == null || particles.Length < system.particleCount) {
			particles = new ParticleSystem.Particle[system.particleCount];
		}
		int count = system.GetParticles(particles);
		for (int i = 0; i < count; ++i) {
			particles[i].remainingLifetime = f;
		}

		system.SetParticles(particles, count);
	}
}
