using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LaserKakerlake_PatrolState : IState
{
    public GameObject enemy;
    public LaserKakerLake_ScriptableObject enemyData;
    public Transform[] patrolPoints;
    int patrolPointIndex;

    NavMeshAgent navMeshAgent;
    bool isRunning;

    public LaserKakerlake_PatrolState(GameObject enemy, LaserKakerLake_ScriptableObject enemyData)
    {
        this.enemy = enemy;
        this.enemyData = enemyData;


        patrolPoints = enemy.GetComponent<LaserKakerlake_Controller>().patrolPoints;
        navMeshAgent = enemy.GetComponent<NavMeshAgent>();

        patrolPointIndex = 0;
    }

    public void Enter()
    {
        navMeshAgent.speed = enemyData.movementSpeed;
        navMeshAgent.SetDestination(patrolPoints[patrolPointIndex].position);
        isRunning = true;
    }
    public void Run()
    {
        if(Vector3.Distance(enemy.transform.position, patrolPoints[patrolPointIndex].position) < 0.5f)
        {
            patrolPointIndex++;
            if(patrolPointIndex > patrolPoints.Length-1)
            {
                patrolPointIndex = 0;
            }
        }
        navMeshAgent.SetDestination(patrolPoints[patrolPointIndex].position);
    }

    public void Exit()
    {
        navMeshAgent.isStopped = true;
        isRunning = false;
    }

    public bool IsRunning()
    {
        return isRunning;
    }

}
