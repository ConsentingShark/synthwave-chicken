using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LaserKakerlake_RunTowardsState : IState
{
    LaserKakerLake_ScriptableObject enemyData;
    GameObject enemy;
    GameObject player;
    ParticleSystem[] flameParticle;

    NavMeshAgent navMeshAgent;
    Rigidbody rb;


    bool isDone;
    bool isRunning;

    public LaserKakerlake_RunTowardsState(LaserKakerLake_ScriptableObject enemyData, GameObject enemy, GameObject player)
    {
        this.enemyData = enemyData;
        this.enemy = enemy;
        this.player = player;


        navMeshAgent = enemy.GetComponent<NavMeshAgent>();
        rb = enemy.GetComponent<Rigidbody>();
        flameParticle = enemy.GetComponent<LaserKakerlake_Controller>().flameParticles;

    }

    public void Enter()
    {
        isDone = false;
        isRunning = true;
        navMeshAgent.speed = enemyData.movementSpeed;
        if (!navMeshAgent.isOnNavMesh) {
            Debug.LogError($"Couldn't activate AI of {enemy.name}. Agent is not on NavMesh.");
        }
        else {
            navMeshAgent.isStopped = false;
        }
        rb.isKinematic = true;

        foreach (ParticleSystem p in flameParticle)
        {
            var emis = p.emission;
            emis.rateOverTime = enemyData.movementFlameParticleEmissionRate;
        }

    }

    public void Run()
    {
        navMeshAgent.SetDestination(player.transform.position);
    }

    public void Exit()
    {
        navMeshAgent.isStopped = true;
        isRunning = false;
    }

    public bool StateIsDone()
    {
        return isDone;
    }

    public bool StateIsRunning()
    {
        return isRunning;
    }
}
