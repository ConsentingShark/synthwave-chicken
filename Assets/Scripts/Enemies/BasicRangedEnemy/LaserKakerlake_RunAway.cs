using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LaserKakerlake_RunAway : IState
{
    LaserKakerLake_ScriptableObject enemyData;
    GameObject enemy;
    GameObject player;

    ParticleSystem[] flameParticle;

    NavMeshAgent navMeshAgent;
    Rigidbody rb;

    Vector3 runAwayDirection;
    float currentRunAwayTime;

    bool isDone;
    bool isRunning;
    float savedAcceleration;

    public LaserKakerlake_RunAway(LaserKakerLake_ScriptableObject enemyData, GameObject enemy, GameObject player)
    {
        this.enemyData = enemyData;
        this.enemy = enemy;
        this.player = player;

        navMeshAgent = enemy.GetComponent<NavMeshAgent>();
        rb = enemy.GetComponent<Rigidbody>();
        flameParticle = enemy.GetComponent<LaserKakerlake_Controller>().flameParticles;

    }

    public void Enter()
    {
        isDone = false;
        isRunning = true;
        navMeshAgent.speed = enemyData.runAwayMovementSpeed;
        navMeshAgent.isStopped = false;
        rb.isKinematic = true;

        currentRunAwayTime = enemyData.runAwayTime;
        savedAcceleration = navMeshAgent.acceleration;
        navMeshAgent.acceleration = enemyData.runAwayAccelerationSpeed;

        foreach (ParticleSystem p in flameParticle)
        {
            var emis = p.emission;
            emis.rateOverTime = enemyData.movementFlameParticleEmissionRate;
        }
    }

    public void Run()
    {
        runAwayDirection = -(player.transform.position - enemy.transform.position).normalized;
        navMeshAgent.SetDestination(runAwayDirection * 10f);

        if(currentRunAwayTime < 0)
        {
            isDone = true;
        }
        else
        {
            currentRunAwayTime -= Time.deltaTime;
        }

    }

    public void Exit()
    {
        navMeshAgent.acceleration = savedAcceleration;
        navMeshAgent.isStopped = true;
        isRunning = false;
    }

    public bool StateIsDone()
    {
        return isDone;
    }

    public bool StateIsRunning()
    {
        return isRunning;
    }
}
