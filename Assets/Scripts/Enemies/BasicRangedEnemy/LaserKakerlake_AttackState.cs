using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LaserKakerlake_AttackState : IState
{
    LaserKakerLake_ScriptableObject enemyData;
    GameObject enemy;
    GameObject player;
    ParticleSystem[] flameParticle;

    NavMeshAgent navMeshAgent;
    float savedAngualrSpeed;

    Vector3 lookDirection;
    LayerMask layerMask;

    //LaserLeft
    GameObject laserLeft;
    GameObject laserLeftParticle;

    //LaserRight
    GameObject laserRight;
    GameObject laserRightParticle;

    float laserSize;

    float currentBeforeAttackTimer;
    float currentAfterAttackTimer;

    bool hasAttacked;
    bool hasHited;

    public bool isDone;
    public bool isRunning;

    private Vector3 rotVelocity;
    private float smoothRot = 0.1f;

    RangeAttackLaser rangeAttackLaser;

    public SomeInsaneLaserSoundControllerSaviourBullshitteryBlackMagic silscsbbm;

    public LaserKakerlake_AttackState(LaserKakerLake_ScriptableObject enemyData, GameObject enemy, GameObject player)
    {
        this.enemyData = enemyData;
        this.enemy = enemy;
        this.player = player;

        navMeshAgent = enemy.GetComponent<NavMeshAgent>();

        laserLeft = enemy.GetComponent<LaserKakerlake_Controller>().laserLeft;
        laserLeft.SetActive(false);

        laserRight = enemy.GetComponent<LaserKakerlake_Controller>().laserRight;
        laserRight.SetActive(false);

        flameParticle = enemy.GetComponent<LaserKakerlake_Controller>().flameParticles;
        rangeAttackLaser = enemy.GetComponentInChildren<RangeAttackLaser>();

        layerMask = enemy.GetComponent<LaserKakerlake_Controller>().laserLayerMask;

        laserLeftParticle = enemy.GetComponent<LaserKakerlake_Controller>().laserLeftParticle;
        laserRightParticle = enemy.GetComponent<LaserKakerlake_Controller>().laserRightParticle;

        silscsbbm = enemy.GetComponent<SomeInsaneLaserSoundControllerSaviourBullshitteryBlackMagic>();

    }

    public void Enter()
    {
        // Anfang Charge
        silscsbbm.BeginChargee();

        isDone = false;
        isRunning = true;

        hasAttacked = false;
        hasHited = false;
        currentBeforeAttackTimer = enemyData.timeBeforeAttack;
        currentAfterAttackTimer = enemyData.timeAfterAttackCooldown;

        laserLeft.SetActive(true);
        laserRight.SetActive(true);
        laserLeftParticle.SetActive(true);
        laserRightParticle.SetActive(true);


        savedAngualrSpeed = navMeshAgent.angularSpeed;
        navMeshAgent.angularSpeed = 0;

        laserSize = 0f;

        foreach(ParticleSystem p in flameParticle)
        {
            var emis = p.emission;
            emis.rateOverTime = enemyData.attackFlameParticleEmissionRate;
        }


    }

    public void Run()
    {
        lookDirection = (player.transform.position - enemy.transform.position).normalized;
        MoveSparksAndScale(laserLeft,laserLeftParticle);
        MoveSparksAndScale(laserRight,laserRightParticle);
        if (!hasAttacked)
        {

            if(currentBeforeAttackTimer < 0)
            {
                hasAttacked = true;

                // Charge Abgebrochen
                silscsbbm.EndLaser();
                laserLeft.SetActive(false);
                laserRight.SetActive(false);
                laserLeftParticle.SetActive(false);
                laserRightParticle.SetActive(false);
            }
            else
            {
                if(currentBeforeAttackTimer >= enemyData.timeBeforeLaser)
                {
                    laserSize += enemyData.laserWidthIncreasingRate * Time.deltaTime;
                    laserLeft.transform.localScale = new Vector3(laserSize, laserLeft.transform.localScale.y, laserSize);
                    laserRight.transform.localScale = new Vector3(laserSize, laserRight.transform.localScale.y, laserSize);
                }
                else
                {
                    //StaysInPosition with Laser
                    silscsbbm.FullLaser();
                    laserLeft.transform.localScale = new Vector3(enemyData.laserEndWidth, laserLeft.transform.localScale.y, enemyData.laserEndWidth);
                    laserRight.transform.localScale = new Vector3(enemyData.laserEndWidth, laserRight.transform.localScale.y, enemyData.laserEndWidth);
     

                    RaycastHit hit;
                    if (!hasHited && Physics.SphereCast(enemy.transform.position, enemyData.laserEndWidth * 2 ,enemy.transform.forward, out hit, 100, layerMask))
                    {
                        if (hit.collider.gameObject.CompareTag("Player"))
                        {
                            hit.collider.gameObject.GetComponent<Data.HealthComponent>().DealDamage(enemyData.damageValue, DamageType.Laser);
                            hasHited = true;
                        }
                    }

                }
                //FollowsPlayer
                if (currentBeforeAttackTimer > enemyData.stopRotatingTime)
                {
                    enemy.transform.forward = Vector3.SmoothDamp(enemy.transform.forward,lookDirection,ref rotVelocity, smoothRot);
                }
                currentBeforeAttackTimer -= Time.deltaTime;
            }
        }
        else
        {

            if (currentAfterAttackTimer < 0)
            {
                isDone = true;
            }
            else
            {
                currentAfterAttackTimer -= Time.deltaTime;
            }
        }

    }

    public void Exit()
    {
        navMeshAgent.angularSpeed = savedAngualrSpeed;
        isRunning = false;
    }

    public bool StateIsDone()
    {
        return isDone;
    }

    public bool StateIsRunning()
    {
        return isRunning;
    }

    private void MoveSparksAndScale(GameObject laserPos,GameObject laserParticles)
    {
        RaycastHit hit;
        Vector3 direction = Quaternion.AngleAxis(-90, Vector3.up) * laserPos.transform.forward;
        Vector3 startPos = laserPos.transform.position;
        startPos.y = laserPos.transform.position.y;
        if (Physics.Raycast(startPos, laserPos.transform.up, out hit, 100f, layerMask))
        {
            //laserParticles.transform.position = player.transform.position + (hit.distance * laserPos.transform.forward) - 0.1f * laserPos.transform.forward;
            laserParticles.transform.position = hit.point;
            laserParticles.transform.rotation = Quaternion.Euler(hit.normal);

            Vector3 scale = laserPos.transform.localScale;
            scale.y = hit.distance;
            laserPos.transform.localScale = scale;
        }
        else
        {
            Vector3 scale = laserPos.transform.localScale;
            scale.y = 100f;
            laserPos.transform.localScale = scale;
        }
    }
}
