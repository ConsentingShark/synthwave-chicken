using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_StateMashine
{
    public IState currentState;
    public IState previouseState;


    public void ChangeState(IState newState)
    {
        if (this.currentState != null)
        {
            this.currentState.Exit();
        }
        this.previouseState = this.currentState;
        this.currentState = newState;
        this.currentState.Enter();
    }

    public void SetPrevioseState()
    {
        currentState = previouseState;
    }

    public void ExecuteState()
    {
        this.currentState.Run();
    }

}
