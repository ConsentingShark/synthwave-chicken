using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChefAnimationEventHandler : MonoBehaviour
{
    [SerializeField] private Zergling_Controller zerglingController;
    [SerializeField] private SoundMap Swing;
    [SerializeField] private SoundMap Laser;
    [SerializeField] private TrailRenderer trail;
    private Animator ac;
    private int acParamIndex;

    private AudioBuffer buffer;

    private bool hasSwing;
    private bool hasLaser;

    private void Start() {
        buffer = new AudioBuffer(2, gameObject);
        ac = GetComponent<Animator>();
        acParamIndex = Animator.StringToHash(Zergling_Controller.inverseMovementVelocityParameter);
        hasSwing = Swing != null;
        hasLaser = Laser != null;
    }

    public void DamageEvent()
    {
        float f = ac.GetFloat(acParamIndex);
        if (f < 0.5f) return;
        zerglingController.DealDamage();
        if (hasSwing) Swing.Play(buffer.Get());
        if (hasLaser) Laser.Play(buffer.Get());
    }

    public void AttackStart() {
        trail.emitting = true;
    }

    public void AttackEnd() {
        trail.emitting = false;
    }
}
