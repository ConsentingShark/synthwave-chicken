using System;
using UnityEngine;
using UnityEngine.AI;

namespace Enemies
{
    public class DeactivatableAIComponent : MonoBehaviour
    {
        private NavMeshAgent _navMeshAgent;
        [SerializeField] public Animator animator;
        [SerializeField] public GlobalEnemySettings settings;
        private bool wasAIActiveBefore = true;
    
        // Start is called before the first frame update
        void Start()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            ActivateAI();
        }

        private void OnEnable()
        {
            ActivateAI();
        }

        public void DeactivateAI()
        {
            if (_navMeshAgent != null)
            {
                _navMeshAgent.isStopped = true;
                if (animator != null)
                {
                    animator.SetBool("isDeactivated", true);
                }
            }
        }

        public void ActivateAI()
        {
            if (_navMeshAgent != null)
            {
                if (!_navMeshAgent.isOnNavMesh) {
                    Debug.LogError($"Couldn't activate AI of {gameObject.name}. Agent is not on NavMesh.");
                    return;
                }
                _navMeshAgent.isStopped = false;
                if (animator != null)
                {
                    animator.SetBool("isDeactivated", false);
                }
            }
        }
        
        public void ToggleAI()
        {
            settings.isAIActive = !settings.isAIActive;
        }

        public void Update()
        {
            if (wasAIActiveBefore != settings.isAIActive)
            {
                wasAIActiveBefore = settings.isAIActive;
                if (settings.isAIActive)
                {
                    ActivateAI();
                }
                else
                {
                    DeactivateAI();
                }
            }
        }
    }
}
