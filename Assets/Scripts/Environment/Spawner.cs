using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class Spawner : MonoBehaviour {

	[SerializeField] private Transform spawnedPrefab;
	[SerializeField] private int spawns;

	[SerializeField] private QuestObejctive objective;

	[SerializeField] private float spawnDelay;
	[SerializeField] private float spawnDelayVariance;

	[SerializeField] private Transform[] spawnPoints;

	[SerializeField] private bool spawnWaves = false;

	[SerializeField] private bool startInstant = false;

	[SerializeField] private UnityEvent OnDone;

	private int kills;

	public void StartSpawning() {
		StartCoroutine(Run());
	}

	private IEnumerator Run() {
		bool hasQuest = objective != null && !objective.Completed;
		if (!startInstant) yield return new WaitForSeconds(spawnDelay + Random.Range(-1, 1) * spawnDelayVariance);

		for (int s = 0; s < spawns; ++s) {

			if (spawnWaves) {
				for (int i = 0; i < spawnPoints.Length; ++i) {
					spawnAt(i, false);
				}
			}
			else {
				int spawnPointIndex = Random.Range(0, spawnPoints.Length);
				spawnAt(spawnPointIndex, hasQuest);
			}


			yield return new WaitForSeconds(spawnDelay + Random.Range(-1, 1) * spawnDelayVariance);
		}

		if (spawnWaves) {
			if (hasQuest) objective.Progress();
			OnDone.Invoke();
		}
	}

	private void spawnAt(int spawnPointIndex, bool hookEvent) {
		Vector3 direction = spawnPoints[spawnPointIndex].forward;
		direction.y = 0;

		var spawned = Instantiate(spawnedPrefab.gameObject);

		bool hasNma = spawned.TryGetComponent(out NavMeshAgent nma);

		if (hasNma) {
			nma.Warp(spawnPoints[spawnPointIndex].position);
		}
		else {
			spawned.transform.position = spawnPoints[spawnPointIndex].position;
		}
		spawned.transform.rotation = Quaternion.LookRotation(direction);

		if (!hookEvent) return;

		var hc = spawned.GetComponentInChildren<HealthComponent>();
		if (hc == null) {
			if (spawned.TryGetComponent(out MobSpawner spawner)) {
				spawner.ProcessSpawned = (spawned) => {
					if (spawned.TryGetComponent(out HealthComponent hc)) {
						hc.OnDamage += MakeProgress;
					}
				};
			}
		}
		else {
			hc.OnDamage += MakeProgress;
		}
	}

	private void MakeProgress(int _, bool died, DamageType __ = DamageType.Unset) {
		if (died) ++kills;
		if (kills == spawns) {
			objective.Progress();
			OnDone.Invoke();
		}
	}
}
