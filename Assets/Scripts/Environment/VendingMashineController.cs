using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendingMashineController : MonoBehaviour
{
    public Data.HealthComponent healthComponent;
    public GameObject chickenCoke;
    public Transform spawnPoint;
    AudioSource audioSource;
    public int value;

    // Start is called before the first frame update
    void Awake()
    {
        healthComponent = GetComponent<Data.HealthComponent>();
        audioSource = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        healthComponent.OnDamage += OnDamage;
    }

    private void OnDisable()
    {
        healthComponent.OnDamage -= OnDamage;
    }


    public void OnDamage(int damage,bool died, DamageType _ = DamageType.Unset)
    {
        Debug.Log("Miep");
        if (value > 0)
        {
            audioSource.Play();
            GameObject g = Instantiate(chickenCoke, spawnPoint.position, Quaternion.identity);
            float force = Random.Range(5, 8);
            g.GetComponent<Rigidbody>().AddForce(spawnPoint.forward * force, ForceMode.Impulse);
            value--;
        }
    }


}
