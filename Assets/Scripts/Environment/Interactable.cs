using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour {
	public delegate void voidDelegate();
	public UnityEvent UnityInteractions;
	public event voidDelegate OnInteract;

	public QuestObejctive requiresComplete;

	public bool interactable = true;

	private void Awake() {
		if (requiresComplete != null) {
			interactable = false;
			requiresComplete.OnComplete += () => {
				interactable = true;
			};
		}
	}

	public void Interact() {
		if (!interactable) return;
		UnityInteractions.Invoke();
		OnInteract?.Invoke();
	}

	public void DestroySelf(bool alsoGameObject) {
		DestroySelf(alsoGameObject, 0);
	}

	public void DestroySelf(bool alsoGameObject, float delay) {
		if (alsoGameObject) {
			Destroy(gameObject, delay);
		}
		else {
			Destroy(this, delay);
		}
	}

	public void DestroySelfGameObject(float delay) {
		DestroySelf(true, delay);
	}
}
