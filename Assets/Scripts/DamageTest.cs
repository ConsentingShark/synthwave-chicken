using Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTest : MonoBehaviour
{
	[SerializeField] public float rad;
	[SerializeField] public int periodic;
	[SerializeField] public float knockback;
	[SerializeField] public int damage = 1;

	private int currentPeriod;

	private void Update() {
		if (periodic > 0) {
			currentPeriod--;
			if (currentPeriod <= 0) {
				currentPeriod = periodic;
			}
			else {
				return;
			}
		}
		var hits = Physics.OverlapSphere(transform.position, rad);
		foreach (var c in hits) {
			var hc = c.GetComponent<HealthComponent>();
			if (hc == null) continue;
			if (hc.DealDamage(damage).Item1) {
				hc.KnockBack(transform.position, knockback);
			}
		}
	}

	private void OnDrawGizmos() {
		Gizmos.DrawWireSphere(transform.position, rad);
	}
}
