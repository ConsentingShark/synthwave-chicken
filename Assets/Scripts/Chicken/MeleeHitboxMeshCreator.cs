using System.Collections.Generic;
using UnityEngine;

public class MeleeHitboxMeshCreator : MonoBehaviour {
	[SerializeField] private float angle;
	[SerializeField] private float thickness;
	[SerializeField] private float radius;
	[SerializeField] private int edgeResolution;

	private void Awake() {
		Recreate();
	}

	[ContextMenu("Recreate")]
	public void Recreate() {
		MeshCollider mcd = GetComponent<MeshCollider>();
		MeshFilter mf = GetComponent<MeshFilter>();

		Mesh mesh = new Mesh();
		List<Vector3> verticies = new List<Vector3>(edgeResolution + 1);
		List<int> triangles = new List<int>(edgeResolution + 1);

		Pie(Vector3.up * thickness, verticies, triangles, true);
		Pie(-Vector3.up * thickness, verticies, triangles, false);

		int offset = verticies.Count / 2;
		for (int i = 0; i < offset - 1; ++i) {
			triangles.Add(i);
			triangles.Add(i + offset);
			triangles.Add(i + 1);

			triangles.Add(i + 1);
			triangles.Add(i + offset);
			triangles.Add(i + 1 + offset);
		}

		triangles.Add(offset - 1);
		triangles.Add(offset - 1 + offset);
		triangles.Add(0);

		triangles.Add(0);
		triangles.Add(offset - 1 + offset);
		triangles.Add(offset);

		mesh.vertices = verticies.ToArray();
		mesh.triangles = triangles.ToArray();

		mesh.RecalculateNormals();

		if (mf != null) {
			mf.mesh = mesh;
		}

		mcd.sharedMesh = mesh;
	}

	private void Pie(in Vector3 offset, List<Vector3> verticies, List<int> triangles, bool orientation) {
		int vertexOffset = verticies.Count;
		Vector3 rad = Vector3.forward * radius;
		rad = Quaternion.Euler(0, -angle / 2, 0) * rad;

		Quaternion q = Quaternion.Euler(0, angle / (edgeResolution - 1), 0);

		verticies.Add(offset);
		verticies.Add(rad + offset);

		for (int i = 1; i < edgeResolution; ++i) {
			rad = q * rad;

			verticies.Add(rad + offset);

			triangles.Add(vertexOffset);
			if (orientation) {
				triangles.Add(vertexOffset + i);
				triangles.Add(vertexOffset + i + 1);
			}
			else {
				triangles.Add(vertexOffset + i + 1);
				triangles.Add(vertexOffset + i);
			}
		}

	}

}
