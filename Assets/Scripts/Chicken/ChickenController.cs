using Data;
using Data.Player;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class ChickenController : MonoBehaviour, ChickenInput {
	[SerializeField] private PlayerStats stats;

	[SerializeField] private float moveVelocity = 1;

	private Vector3 moveControl;
	private Vector3 mouseDirection;
	private Vector3 stickDirection;
	private Vector3 lastAimedDirection;
	private bool stickAiming;

	private Vector3 walkingDirection;
	private Vector3 walkingVelocity;
	private Vector3 moveDampVelocity;
	[SerializeField] private float moveTime;
	[SerializeField] private Animator ac;

	private Rigidbody rb;
	private CharacterController cc;

	private bool aiming = false;

	private Vector3 modelDirectionBlendVelocity;
	[SerializeField] private float modelDirectionBlendTimeAim;
	[SerializeField] private float modelDirectionBlendTimeWalk;

	private float attackLayerBlend;
	private float attackLayerBlendVelocity;
	[SerializeField] private float attackLayerBlendTimeAttack;
	[SerializeField] private float attackLayerBlendTimeRelease;

	private float walkingOverrideLayerBlend;
	private float walkingOverrideLayerVelocity;
	[SerializeField] private float walkingOverrideLayerBlendTime = 0.1f;

	[SerializeField] private Transform model;

	private int acAttackLayerIndex;
	private int acWalkingOverrideLayerIndex;

	private Camera cam;

	[SerializeField] private Transform projectileOrigin;
	[SerializeField] private ParticleSystem projectileEmitter;

	[SerializeField] private float attackRange = 2.3f;
	private float KnockbackStrength => stats.meleeKnockback;

	[SerializeField] private Collider[] meleeHitbox;

	private readonly Collider[] hits = new Collider[512];

	private bool gunReady = true;
	[SerializeField] private float gunAnimationCooldown = 0.15f;

	[SerializeField] private PlayerHeat heat;
	private int HeatIncrement => stats.gunHeat;

	[SerializeField] private GameObject ragdollPrefab;
	[SerializeField] private Transform sword;
	[SerializeField] private Transform gun;

	private Vector3 knockbackForce;
	[Range(0.03f, 1)]
	[SerializeField] private float knockBackFalloff = 0.5f;

	[SerializeField] private float verticalRaycastOffset;

	[SerializeField] private float interactionRange;
	[SerializeField] private LayerMask interactLayers;

	private bool useGamepad;

	private Collider[] colliderCache = new Collider[16];

	public Transform Model => model;

	public bool Moving => walkingVelocity.sqrMagnitude > 0.01f;

	[SerializeField] private PauseMenu pauseMenu;


	private bool shooting;
	private bool swinging;


	public bool AllowWalk { get; set; } = true;
	public bool AllowMelee { get; set; } = true;
	public bool AllowGun { get; set; } = true;


	[SerializeField] private SoundMap[] swordSwings;
	[SerializeField] private SoundMap gunFire;


	private AudioBuffer sources;

	private void Awake() {
		sources = new AudioBuffer(8, gameObject);
	}




	[ContextMenu("KILL")]
	private void Kill() {
		GameObject ragdoll = Instantiate(ragdollPrefab);
		Debug.Log(model.transform.position);

		var currentBones = model.GetComponentsInChildren<Transform>();
		var newBones = ragdoll.GetComponentsInChildren<Transform>();
		int off = 0;
		for (int i = 1; i < newBones.Length; ++i) {
			while (currentBones[i + off].gameObject.name != newBones[i].gameObject.name) ++off;
			newBones[i].localPosition = currentBones[off + i].localPosition;
			newBones[i].localRotation = currentBones[off + i].localRotation;
		}
		ragdoll.transform.position = model.transform.position;
		ragdoll.transform.rotation = model.transform.rotation;
		Vector3 velo = walkingVelocity + knockbackForce;
		var rigids = ragdoll.GetComponentsInChildren<Rigidbody>();
		foreach (var rigid in rigids) {
			rigid.velocity = velo;
		}

		model.gameObject.SetActive(false);
	}

	private HealthComponent hc;

	private void Start() {
		InputManager.Instance.chickenInput = this;
		useGamepad = InputManager.Instance.PlayerUseGamePad();
		rb = GetComponent<Rigidbody>();
		cc = GetComponent<CharacterController>();
		acAttackLayerIndex = ac.GetLayerIndex("Attacking");
		acWalkingOverrideLayerIndex = ac.GetLayerIndex("Walk Override");
		cam = Camera.main;

		hc = GetComponent<HealthComponent>();
		if (hc != null) {
			hc.OnDamage += HandleDamage;
			hc.OnKnockback += HandleKnockback;
		}
	}

	private bool isDead;
	private bool killed;

	private const string trgAttack = "Attack";
	private const string trgShoot = "Shoot";

	void HandleDamage(int _, bool died, DamageType __ = DamageType.Unset) {
		if (isDead) return;
		isDead = died;

		if (isDead)
		{
			InputManager.Instance.DisablePlayerControl();
			StartCoroutine(GameOver());
		}
	}

	public IEnumerator GameOver()
	{
		yield return new WaitForSeconds(5);
		pauseMenu.DeathScreen();
	}

	private void HandleKnockback(Vector3 origin, float strength) {
		Vector3 kb = (transform.localPosition - origin).normalized * strength;
		kb.y = 0;
		Knockback(kb);
		if (isDead) {
			hc.OnDamage -= HandleDamage;
			hc.OnKnockback -= HandleKnockback;
			Destroy(hc);
		}
	}
	private void LateUpdate() {
		if (killed) return;
		if (!isDead) return;
		Kill();
		killed = true;
	}

	public void UpdateMoveControl(InputAction.CallbackContext x) {
		SetMoveControl(x.ReadValue<Vector2>());
	}

	public void UpdateCursorPosition(InputAction.CallbackContext x) {
		Vector2 mp = x.ReadValue<Vector2>();
		Ray r = cam.ScreenPointToRay(mp);
		Plane p = new Plane(Vector3.up, transform.position + Vector3.up * verticalRaycastOffset);
		if (p.Raycast(r, out float enter)) {
			Vector3 hp = r.origin + enter * r.direction;
			mouseDirection = hp - transform.position;
			mouseDirection.y = 0;
		}
	}

	public void UpdateAimDirection(InputAction.CallbackContext x) {
		Vector2 d = x.ReadValue<Vector2>();
		Vector3 d3 = new Vector3(d.x, 0, d.y).normalized;
		if (d3.sqrMagnitude > 0) {
			stickDirection = d3;
			stickAiming = true;
		}
		else {
			stickAiming = false;
		}
	}

	public void UpdateAttackCommand(InputAction.CallbackContext x) {
		if (!swinging) {
			swinging = x.started;
		}
		else if (x.canceled) {
			swinging = false;
		}
		//if (!x.started) return;
		//ac.SetTrigger(trgAttack);
	}

	public void UpdateShootCommand(InputAction.CallbackContext x) {
		if (!shooting) {
			shooting = x.started;
		}
		else if (x.canceled) {
			shooting = false;
		}
		//if (!x.started) return;
		//if (heat.overheated) return;
		//ac.SetTrigger(trgShoot);
	}

	public void UpdateControlScheme(PlayerInput input) {
		useGamepad = InputManager.Instance.PlayerUseGamePad();
	}

	public void UpdateInteract(InputAction.CallbackContext x) {
		if (!x.started) return;
		int count = Physics.OverlapSphereNonAlloc(transform.position + verticalRaycastOffset * Vector3.up, interactionRange, colliderCache, interactLayers.value);
		if (count == 0) return;
		for (int i = 0; i < count; ++i) {
			Interactable ic = colliderCache[i].GetComponent<Interactable>();
			if (ic == null) continue;
			ic.Interact();
			return;
		}
	}

	public void CutsceneOverrideViewDirection(Vector3 dir, bool instant) {
		lastAimedDirection = dir;
		modelDirectionBlendVelocity = Vector3.zero;
		if (instant) {
			model.localRotation = Quaternion.LookRotation(dir);
		}
	}

	public void CutsceneOverrideMoveVelocity(Vector2 velocity) {
		SetMoveControl(velocity);
	}

	public void SetMoveControl(in Vector2 v) {
		moveControl = new Vector3(v.x * moveVelocity, 0, v.y * moveVelocity);
	}

	public void Shoot() {
		if (!gunReady) return;

		if (heat.overheated) return;
		heat.IncreaseHeat(HeatIncrement);
		if (heat.overheated) {
			ac.ResetTrigger(trgShoot);
		}

		ac.ResetTrigger(trgShoot);
		projectileEmitter.transform.position = projectileOrigin.transform.position;
		projectileEmitter.transform.rotation = Quaternion.LookRotation(model.forward);
		projectileEmitter.Play();

		float offset = projectileEmitter.emission.GetBurst(0).repeatInterval;
		gunFire.Play(sources.Get());
		gunFire.Play(sources.Get(), offset);
		gunFire.Play(sources.Get(), 2 * offset);



		gunReady = false;
		StartCoroutine(reloadGun());
	}

	private IEnumerator reloadGun() {
		yield return new WaitForSeconds(gunAnimationCooldown);
		gunReady = true;
	}

	public void Melee(int hitboxIndex = 0) {
		ac.ResetTrigger(trgAttack);

		Collider hitbox = meleeHitbox[hitboxIndex];
		Vector3 hitboxPos = hitbox.transform.position;

		foreach (var map in swordSwings) {
			map.Play(sources.Get());
		}



		int hitcount = Physics.OverlapSphereNonAlloc(hitboxPos, attackRange, hits);

		for (int i = 0; i < hitcount; ++i) {
			var other = hits[i];

			if (!Physics.ComputePenetration(
				hitbox, hitboxPos, hitbox.transform.rotation,
				other, other.transform.position, other.transform.rotation,
				out _, out _))
			{
				continue;
			}

			var hc = other.GetComponent<HealthComponent>();
			if (hc == null) continue;

			if (hc.DealDamage(stats.meleeDamage, ~TeamType.Player, DamageType.Sword).Item1) {
				hc.KnockBack(hitboxPos, KnockbackStrength);
			}
		}
	}

	public void Knockback(in Vector3 force) {
		knockbackForce += force;
	}

	private void Update() {
		projectileEmitter.transform.position = projectileOrigin.position;
		projectileEmitter.transform.rotation = Quaternion.LookRotation(model.forward);
	}

	private void FixedUpdate() {
		// UnityEngine.InputSystem.PlayerInput doesnt Invoke the ControlsChanged Event after Scene reloading
		// As a workaround, poll the controll scheme every update
		useGamepad = InputManager.Instance.PlayerUseGamePad();

		if (swinging && AllowMelee) ac.SetTrigger(trgAttack);
		if (shooting && AllowGun) ac.SetTrigger(trgShoot);

		heat.UpdateHeatCooldown();
		if (heat.overheated) {
			ac.ResetTrigger(trgShoot);
		}

		// Movement Velocity
		if (AllowWalk) {
			walkingVelocity = Vector3.SmoothDamp(walkingVelocity, moveControl, ref moveDampVelocity, moveTime);
			cc.SimpleMove(walkingVelocity + knockbackForce);
		}
		else {
			walkingVelocity = Vector3.zero;
		}
		ac.SetFloat("MoveVelocity", walkingVelocity.magnitude / moveVelocity);

		knockbackForce *= knockBackFalloff;


		// Attack handling
		var info = ac.GetCurrentAnimatorStateInfo(acAttackLayerIndex);
		aiming = !info.IsName("Idle");
		if (aiming) {
			attackLayerBlend = Mathf.SmoothDamp(attackLayerBlend, 1, ref attackLayerBlendVelocity, attackLayerBlendTimeAttack);
		}
		else {
			attackLayerBlend = Mathf.SmoothDamp(attackLayerBlend, 0, ref attackLayerBlendVelocity, attackLayerBlendTimeRelease);
		}

		walkingOverrideLayerBlend = Mathf.SmoothDamp(walkingOverrideLayerBlend, walkingVelocity.magnitude / moveVelocity > 0.01f ? 1 : 0, ref walkingOverrideLayerVelocity, walkingOverrideLayerBlendTime);

		ac.SetLayerWeight(acAttackLayerIndex, attackLayerBlend);
		ac.SetLayerWeight(acWalkingOverrideLayerIndex, walkingOverrideLayerBlend);

		// Direction adjustments
		bool walking = Moving;
		if (walking) {
			walkingDirection = walkingVelocity.normalized;
		}
		else {
			walkingDirection = Vector3.zero;
		}

		Vector3 aimDirection = useGamepad ? stickDirection : mouseDirection;

		if ((useGamepad && stickAiming) || (!useGamepad && aiming)) {
			RotateTo(aimDirection, aiming ? modelDirectionBlendTimeAim : modelDirectionBlendTimeWalk);
			lastAimedDirection = aimDirection;
		}
		else if (walkingDirection.sqrMagnitude > 0) {
			RotateTo(walkingDirection, aiming ? modelDirectionBlendTimeAim : modelDirectionBlendTimeWalk);
			lastAimedDirection = walkingDirection;
		}
		else {
			RotateTo(lastAimedDirection, aiming ? modelDirectionBlendTimeAim : modelDirectionBlendTimeWalk);
		}

		if (walking) {
			ac.SetFloat("DirectionOffset", ((Vector3.SignedAngle(walkingDirection, model.forward, Vector3.up) + 360) % 360) / 360.0f);
		}
	}

	private void RotateTo(in Vector3 direction, float time) {
		// Edge Case - Directions are on the same Ray, but opposed in orientation
		if ((model.forward + direction).sqrMagnitude < 0.0001f) {
			model.localRotation = Quaternion.LookRotation(Vector3.SmoothDamp(model.forward, model.right, ref modelDirectionBlendVelocity, time));
		}
		else {
			model.localRotation = Quaternion.LookRotation(Vector3.SmoothDamp(model.forward, direction, ref modelDirectionBlendVelocity, time));
		}
	}

		private void OnDrawGizmosSelected() {
		Gizmos.color = Color.red;
		Gizmos.DrawLine(transform.localPosition, transform.localPosition + model.forward);
	}
}

