Shader "Supreme Chicken Samurai/Minimap Render" {
    Properties{
        _MinimapRender("MinimapRender", 2D) = "white" {}
        [HDR]_Color("Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        _near("near", Float) = 5
        _far("far", Float) = 400
        _width("width", Float) = 5
    }
        SubShader{
            Tags {
                "IgnoreProjector" = "True"
                "Queue" = "Transparent"
                "RenderType" = "Transparent"
            }
            Pass {
                Name "FORWARD"
                Tags {
                    "LightMode" = "ForwardBase"
                }
                Blend SrcAlpha OneMinusSrcAlpha
                ZWrite Off

                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "UnityCG.cginc"
                #pragma multi_compile_fwdbase
                #pragma target 3.0
                uniform sampler2D _MinimapRender; uniform float4 _MinimapRender_ST;
                float FnCheckBorder(float near, float far, float offsetX, float offsetY, float2 uv, float currentDepth) {
                    if (currentDepth > near && currentDepth < far) {
                        return 0;
                    }

                    float depth;

                    // Left
                    depth = LinearEyeDepth(
                        SAMPLE_DEPTH_TEXTURE(
                            _MinimapRender,
                            (float2(-offsetX, 0.0) + uv.rg)
                        ));
                    if (depth > near && depth < far) {
                        return 1;
                    }

                    // Right
                    depth = LinearEyeDepth(
                        SAMPLE_DEPTH_TEXTURE(
                            _MinimapRender,
                            (float2(offsetX, 0.0) + uv.rg)
                        ));
                    if (depth > near && depth < far) {
                        return 1;
                    }

                    // Up
                    depth = LinearEyeDepth(
                        SAMPLE_DEPTH_TEXTURE(
                            _MinimapRender,
                            (float2(0.0, offsetY) + uv.rg)
                        ));
                    if (depth > near && depth < far) {
                        return 1;
                    }

                    // Down
                    depth = LinearEyeDepth(
                        SAMPLE_DEPTH_TEXTURE(
                            _MinimapRender,
                            (float2(0.0, -offsetY) + uv.rg)
                        ));
                    if (depth > near && depth < far) {
                        return 1;
                    }

                    return 0;
                }
                UNITY_INSTANCING_BUFFER_START(Props)
                    UNITY_DEFINE_INSTANCED_PROP(float4, _Color)
                    UNITY_DEFINE_INSTANCED_PROP(float, _near)
                    UNITY_DEFINE_INSTANCED_PROP(float, _far)
                    UNITY_DEFINE_INSTANCED_PROP(float, _width)
                UNITY_INSTANCING_BUFFER_END(Props)
                struct VertexInput {
                    float4 vertex : POSITION;
                    UNITY_VERTEX_INPUT_INSTANCE_ID
                    float2 texcoord0 : TEXCOORD0;
                };
                struct VertexOutput {
                    float4 pos : SV_POSITION;
                    UNITY_VERTEX_INPUT_INSTANCE_ID
                    float2 uv0 : TEXCOORD0;
                };
                VertexOutput vert(VertexInput v) {
                    VertexOutput o = (VertexOutput)0;
                    UNITY_SETUP_INSTANCE_ID(v);
                    UNITY_TRANSFER_INSTANCE_ID(v, o);
                    o.uv0 = v.texcoord0;
                    o.pos = UnityObjectToClipPos(v.vertex);
                    return o;
                }
                float4 frag(VertexOutput i) : COLOR {
                    UNITY_SETUP_INSTANCE_ID(i);
                    //float _near_var =_near;
                    //float _far_var = UNITY_ACCESS_INSTANCED_PROP(Props, _far);
                    //float _width_var = UNITY_ACCESS_INSTANCED_PROP(Props, _width);
                    float4 color = tex2D(_MinimapRender,TRANSFORM_TEX(i.uv0, _MinimapRender));
                    float blend = FnCheckBorder(_near, _far, _width, _width, i.uv0, color.a);
                    //return fixed4(blend, color.g, 0, 1);
                    return fixed4(lerp(color, _Color, blend));
                }
                ENDCG
            }
        }
            FallBack "Diffuse"
}
