// Shader created with Shader Forge v1.40 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.40;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,cpap:True,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33725,y:32718,varname:node_3138,prsc:2|emission-9809-OUT,alpha-7067-OUT,clip-7067-OUT,voffset-2537-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:33058,y:32474,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:True,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.4626424,c3:0,c4:1;n:type:ShaderForge.SFN_Time,id:8095,x:31361,y:32861,varname:node_8095,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:7021,x:31361,y:32749,ptovrint:False,ptlb:NoiseSpeed01,ptin:_NoiseSpeed01,varname:node_7021,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_ValueProperty,id:470,x:31361,y:33058,ptovrint:False,ptlb:NoiseSpeed02,ptin:_NoiseSpeed02,varname:_NoiseSpeed02,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:-0.1;n:type:ShaderForge.SFN_Multiply,id:397,x:31566,y:32715,varname:node_397,prsc:2|A-7021-OUT,B-8095-T;n:type:ShaderForge.SFN_Multiply,id:4905,x:31571,y:33024,varname:node_4905,prsc:2|A-8095-T,B-470-OUT;n:type:ShaderForge.SFN_TexCoord,id:9988,x:31566,y:32548,varname:node_9988,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Tex2d,id:329,x:31985,y:32705,ptovrint:False,ptlb:NoiseTexture01,ptin:_NoiseTexture01,varname:node_329,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-2915-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:2442,x:31984,y:32996,ptovrint:False,ptlb:NoiseTexture02,ptin:_NoiseTexture02,varname:_NoiseTexture02,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-8460-UVOUT;n:type:ShaderForge.SFN_Panner,id:8460,x:31771,y:32998,varname:node_8460,prsc:2,spu:1,spv:0|UVIN-9988-UVOUT,DIST-4905-OUT;n:type:ShaderForge.SFN_Panner,id:2915,x:31772,y:32705,varname:node_2915,prsc:2,spu:1,spv:0|UVIN-9988-UVOUT,DIST-397-OUT;n:type:ShaderForge.SFN_Multiply,id:1429,x:32227,y:32857,varname:node_1429,prsc:2|A-329-RGB,B-2442-RGB;n:type:ShaderForge.SFN_Posterize,id:6823,x:32628,y:32857,varname:node_6823,prsc:2|IN-346-OUT,STPS-4933-OUT;n:type:ShaderForge.SFN_NormalVector,id:1259,x:32735,y:33082,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:4874,x:32963,y:33082,varname:node_4874,prsc:2|A-1259-OUT,B-6823-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3997,x:32963,y:33001,ptovrint:False,ptlb:VertexOffset,ptin:_VertexOffset,varname:node_3997,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:2537,x:33141,y:33082,varname:node_2537,prsc:2|A-3997-OUT,B-4874-OUT;n:type:ShaderForge.SFN_Multiply,id:346,x:32422,y:32857,varname:node_346,prsc:2|A-1429-OUT,B-8556-OUT;n:type:ShaderForge.SFN_Vector1,id:4933,x:32422,y:32996,varname:node_4933,prsc:2,v1:3;n:type:ShaderForge.SFN_ComponentMask,id:8103,x:33021,y:32776,varname:node_8103,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-346-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8556,x:32227,y:32996,ptovrint:False,ptlb:Intensity,ptin:_Intensity,varname:node_8556,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:7;n:type:ShaderForge.SFN_Multiply,id:9809,x:33283,y:32561,varname:node_9809,prsc:2|A-7241-RGB,B-6823-OUT;n:type:ShaderForge.SFN_Clamp01,id:7067,x:33304,y:32802,varname:node_7067,prsc:2|IN-8103-OUT;proporder:7241-7021-470-329-2442-3997-8556;pass:END;sub:END;*/

Shader "Shader Forge/AtomicMushroom" {
    Properties {
        [HDR]_Color ("Color", Color) = (1,0.4626424,0,1)
        _NoiseSpeed01 ("NoiseSpeed01", Float ) = 0.1
        _NoiseSpeed02 ("NoiseSpeed02", Float ) = -0.1
        _NoiseTexture01 ("NoiseTexture01", 2D) = "white" {}
        _NoiseTexture02 ("NoiseTexture02", 2D) = "white" {}
        _VertexOffset ("VertexOffset", Float ) = 0.1
        _Intensity ("Intensity", Float ) = 7
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma target 3.0
            uniform sampler2D _NoiseTexture01; uniform float4 _NoiseTexture01_ST;
            uniform sampler2D _NoiseTexture02; uniform float4 _NoiseTexture02_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float4, _Color)
                UNITY_DEFINE_INSTANCED_PROP( float, _NoiseSpeed01)
                UNITY_DEFINE_INSTANCED_PROP( float, _NoiseSpeed02)
                UNITY_DEFINE_INSTANCED_PROP( float, _VertexOffset)
                UNITY_DEFINE_INSTANCED_PROP( float, _Intensity)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float _VertexOffset_var = UNITY_ACCESS_INSTANCED_PROP( Props, _VertexOffset );
                float _NoiseSpeed01_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed01 );
                float4 node_8095 = _Time;
                float2 node_2915 = (o.uv0+(_NoiseSpeed01_var*node_8095.g)*float2(1,0));
                float4 _NoiseTexture01_var = tex2Dlod(_NoiseTexture01,float4(TRANSFORM_TEX(node_2915, _NoiseTexture01),0.0,0));
                float _NoiseSpeed02_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed02 );
                float2 node_8460 = (o.uv0+(node_8095.g*_NoiseSpeed02_var)*float2(1,0));
                float4 _NoiseTexture02_var = tex2Dlod(_NoiseTexture02,float4(TRANSFORM_TEX(node_8460, _NoiseTexture02),0.0,0));
                float _Intensity_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Intensity );
                float3 node_346 = ((_NoiseTexture01_var.rgb*_NoiseTexture02_var.rgb)*_Intensity_var);
                float node_4933 = 3.0;
                float3 node_6823 = floor(node_346 * node_4933) / (node_4933 - 1);
                v.vertex.xyz += (_VertexOffset_var*(v.normal*node_6823));
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float _NoiseSpeed01_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed01 );
                float4 node_8095 = _Time;
                float2 node_2915 = (i.uv0+(_NoiseSpeed01_var*node_8095.g)*float2(1,0));
                float4 _NoiseTexture01_var = tex2D(_NoiseTexture01,TRANSFORM_TEX(node_2915, _NoiseTexture01));
                float _NoiseSpeed02_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed02 );
                float2 node_8460 = (i.uv0+(node_8095.g*_NoiseSpeed02_var)*float2(1,0));
                float4 _NoiseTexture02_var = tex2D(_NoiseTexture02,TRANSFORM_TEX(node_8460, _NoiseTexture02));
                float _Intensity_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Intensity );
                float3 node_346 = ((_NoiseTexture01_var.rgb*_NoiseTexture02_var.rgb)*_Intensity_var);
                float node_7067 = saturate(node_346.r);
                clip(node_7067 - 0.5);
////// Lighting:
////// Emissive:
                float4 _Color_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Color );
                float node_4933 = 3.0;
                float3 node_6823 = floor(node_346 * node_4933) / (node_4933 - 1);
                float3 emissive = (_Color_var.rgb*node_6823);
                float3 finalColor = emissive;
                return fixed4(finalColor,node_7067);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma target 3.0
            uniform sampler2D _NoiseTexture01; uniform float4 _NoiseTexture01_ST;
            uniform sampler2D _NoiseTexture02; uniform float4 _NoiseTexture02_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float, _NoiseSpeed01)
                UNITY_DEFINE_INSTANCED_PROP( float, _NoiseSpeed02)
                UNITY_DEFINE_INSTANCED_PROP( float, _VertexOffset)
                UNITY_DEFINE_INSTANCED_PROP( float, _Intensity)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float2 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float _VertexOffset_var = UNITY_ACCESS_INSTANCED_PROP( Props, _VertexOffset );
                float _NoiseSpeed01_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed01 );
                float4 node_8095 = _Time;
                float2 node_2915 = (o.uv0+(_NoiseSpeed01_var*node_8095.g)*float2(1,0));
                float4 _NoiseTexture01_var = tex2Dlod(_NoiseTexture01,float4(TRANSFORM_TEX(node_2915, _NoiseTexture01),0.0,0));
                float _NoiseSpeed02_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed02 );
                float2 node_8460 = (o.uv0+(node_8095.g*_NoiseSpeed02_var)*float2(1,0));
                float4 _NoiseTexture02_var = tex2Dlod(_NoiseTexture02,float4(TRANSFORM_TEX(node_8460, _NoiseTexture02),0.0,0));
                float _Intensity_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Intensity );
                float3 node_346 = ((_NoiseTexture01_var.rgb*_NoiseTexture02_var.rgb)*_Intensity_var);
                float node_4933 = 3.0;
                float3 node_6823 = floor(node_346 * node_4933) / (node_4933 - 1);
                v.vertex.xyz += (_VertexOffset_var*(v.normal*node_6823));
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float _NoiseSpeed01_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed01 );
                float4 node_8095 = _Time;
                float2 node_2915 = (i.uv0+(_NoiseSpeed01_var*node_8095.g)*float2(1,0));
                float4 _NoiseTexture01_var = tex2D(_NoiseTexture01,TRANSFORM_TEX(node_2915, _NoiseTexture01));
                float _NoiseSpeed02_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed02 );
                float2 node_8460 = (i.uv0+(node_8095.g*_NoiseSpeed02_var)*float2(1,0));
                float4 _NoiseTexture02_var = tex2D(_NoiseTexture02,TRANSFORM_TEX(node_8460, _NoiseTexture02));
                float _Intensity_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Intensity );
                float3 node_346 = ((_NoiseTexture01_var.rgb*_NoiseTexture02_var.rgb)*_Intensity_var);
                float node_7067 = saturate(node_346.r);
                clip(node_7067 - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
