// Based on http://www.shaderslab.com/demo-93---wireframe.html
Shader "Custom/Geometry/Wireframe"
{
    Properties
    {
        [PowerSlider(3.0)]
        _WireframeVal("Wireframe width", Range(0., 0.5)) = 0.05
        [HDR]_FrontColor("Front color", color) = (1., 1., 1., 1.)
        [HDR]_BackColor("Back color", color) = (1., 1., 1., 1.)
        [Toggle] _RemoveDiag("Remove diagonals?", Float) = 0.

        //[HDR]_FresnelColor("Fresnel Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        //_FresnelExp("Fresnel Exp", Float) = 1
        //_TexScale("Tex Scale", Float) = 1
        //_Velocity("Velocity", Float) = 0
        //[HDR]_FizzColor("Fizz Color", Color) = (0.5,0.5,0.5,1)
        //_Multiplicator("Multiplicator", Float) = 1
        // _node_5484("node_5484", 2D) = "white" {}
    }
        SubShader
        {
            Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }

            /*
            Pass
            {
                Cull Front
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #pragma geometry geom

            // Change "shader_feature" with "pragma_compile" if you want set this keyword from c# code
            #pragma shader_feature __ _REMOVEDIAG_ON

            #include "UnityCG.cginc"

            struct v2g {
                float4 worldPos : SV_POSITION;
            };

            struct g2f {
                float4 pos : SV_POSITION;
                float3 bary : TEXCOORD0;
            };

            v2g vert(appdata_base v) {
                v2g o;
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

            [maxvertexcount(3)]
            void geom(triangle v2g IN[3], inout TriangleStream<g2f> triStream) {
                float3 param = float3(0., 0., 0.);

                #if _REMOVEDIAG_ON
                float EdgeA = length(IN[0].worldPos - IN[1].worldPos);
                float EdgeB = length(IN[1].worldPos - IN[2].worldPos);
                float EdgeC = length(IN[2].worldPos - IN[0].worldPos);

                if (EdgeA > EdgeB && EdgeA > EdgeC)
                    param.y = 1.;
                else if (EdgeB > EdgeC && EdgeB > EdgeA)
                    param.x = 1.;
                else
                    param.z = 1.;
                #endif

                g2f o;
                o.pos = mul(UNITY_MATRIX_VP, IN[0].worldPos);
                o.bary = float3(1., 0., 0.) + param;
                triStream.Append(o);
                o.pos = mul(UNITY_MATRIX_VP, IN[1].worldPos);
                o.bary = float3(0., 0., 1.) + param;
                triStream.Append(o);
                o.pos = mul(UNITY_MATRIX_VP, IN[2].worldPos);
                o.bary = float3(0., 1., 0.) + param;
                triStream.Append(o);
            }

            float _WireframeVal;
            fixed4 _BackColor;

            fixed4 frag(g2f i) : SV_Target {
            if (!any(bool3(i.bary.x < _WireframeVal, i.bary.y < _WireframeVal, i.bary.z < _WireframeVal)))
                 discard;

                return _BackColor;
            }

            ENDCG
        }
        */

        Pass {
            Cull Back
            Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma geometry geom

                // Change "shader_feature" with "pragma_compile" if you want set this keyword from c# code
                #pragma shader_feature __ _REMOVEDIAG_ON

                #include "UnityCG.cginc"

                struct v2g {
                    float4 worldPos : SV_POSITION;
                };

                struct g2f {
                    float4 pos : SV_POSITION;
                    float3 bary : TEXCOORD0;
                };

                v2g vert(appdata_base v) {
                    v2g o;
                    o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                    return o;
                }

                [maxvertexcount(3)]
                void geom(triangle v2g IN[3], inout TriangleStream<g2f> triStream) {
                    float3 param = float3(0., 0., 0.);

                    #if _REMOVEDIAG_ON
                    float EdgeA = length(IN[0].worldPos - IN[1].worldPos);
                    float EdgeB = length(IN[1].worldPos - IN[2].worldPos);
                    float EdgeC = length(IN[2].worldPos - IN[0].worldPos);

                    if (EdgeA > EdgeB && EdgeA > EdgeC)
                        param.y = 1.;
                    else if (EdgeB > EdgeC && EdgeB > EdgeA)
                        param.x = 1.;
                    else
                        param.z = 1.;
                    #endif

                    g2f o;
                    o.pos = mul(UNITY_MATRIX_VP, IN[0].worldPos);
                    o.bary = float3(1., 0., 0.) + param;
                    triStream.Append(o);
                    o.pos = mul(UNITY_MATRIX_VP, IN[1].worldPos);
                    o.bary = float3(0., 0., 1.) + param;
                    triStream.Append(o);
                    o.pos = mul(UNITY_MATRIX_VP, IN[2].worldPos);
                    o.bary = float3(0., 1., 0.) + param;
                    triStream.Append(o);
                }

                float _WireframeVal;
                fixed4 _FrontColor;
                fixed4 _BackColor;

                fixed4 frag(g2f i) : SV_Target {
                    if (!any(bool3(i.bary.x <= _WireframeVal, i.bary.y <= _WireframeVal, i.bary.z <= _WireframeVal))) {
                        return _BackColor;
                    }

                    return _FrontColor;
                }

                ENDCG
            }

        //Pass {
        //    Name "FORWARD"
        //    Tags {
        //        "LightMode" = "ForwardBase"
        //    }
        //    Blend One One
        //    ZWrite Off

        //    CGPROGRAM
        //    #pragma vertex vert
        //    #pragma fragment frag
        //    #pragma multi_compile_instancing
        //    #include "UnityCG.cginc"
        //    #pragma multi_compile_fwdbase
        //    #pragma target 3.0
        //    uniform sampler2D _node_5484; uniform float4 _node_5484_ST;
        //    UNITY_INSTANCING_BUFFER_START(Props)
        //        UNITY_DEFINE_INSTANCED_PROP(float4, _FresnelColor)
        //        UNITY_DEFINE_INSTANCED_PROP(float, _FresnelExp)
        //        UNITY_DEFINE_INSTANCED_PROP(float, _TexScale)
        //        UNITY_DEFINE_INSTANCED_PROP(float, _Velocity)
        //        UNITY_DEFINE_INSTANCED_PROP(float4, _FizzColor)
        //        UNITY_DEFINE_INSTANCED_PROP(float, _Multiplicator)
        //    UNITY_INSTANCING_BUFFER_END(Props)
        //    struct VertexInput {
        //        UNITY_VERTEX_INPUT_INSTANCE_ID
        //        float4 vertex : POSITION;
        //        float3 normal : NORMAL;
        //    };
        //    struct VertexOutput {
        //        float4 pos : SV_POSITION;
        //        UNITY_VERTEX_INPUT_INSTANCE_ID
        //        float4 posWorld : TEXCOORD0;
        //        float3 normalDir : TEXCOORD1;
        //    };
        //    VertexOutput vert(VertexInput v) {
        //        VertexOutput o = (VertexOutput)0;
        //        UNITY_SETUP_INSTANCE_ID(v);
        //        UNITY_TRANSFER_INSTANCE_ID(v, o);
        //        o.normalDir = UnityObjectToWorldNormal(v.normal);
        //        o.posWorld = mul(unity_ObjectToWorld, v.vertex);
        //        o.pos = UnityObjectToClipPos(v.vertex);
        //        return o;
        //    }
        //    float4 frag(VertexOutput i) : COLOR {
        //        UNITY_SETUP_INSTANCE_ID(i);
        //        i.normalDir = normalize(i.normalDir);
        //        float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
        //        float3 normalDirection = i.normalDir;
        //        ////// Lighting:
        //        ////// Emissive:
        //                        float4 _FizzColor_var = UNITY_ACCESS_INSTANCED_PROP(Props, _FizzColor);
        //                        float _Velocity_var = UNITY_ACCESS_INSTANCED_PROP(Props, _Velocity);
        //                        float4 node_7185 = _Time;
        //                        float _TexScale_var = UNITY_ACCESS_INSTANCED_PROP(Props, _TexScale);
        //                        float2 node_8658 = float2(0.0,((_Velocity_var * node_7185.g) + (_TexScale_var * i.posWorld.g)));
        //                        float4 _node_5484_var = tex2D(_node_5484,TRANSFORM_TEX(node_8658, _node_5484));
        //                        float4 _FresnelColor_var = UNITY_ACCESS_INSTANCED_PROP(Props, _FresnelColor);
        //                        float _FresnelExp_var = UNITY_ACCESS_INSTANCED_PROP(Props, _FresnelExp);
        //                        float _Multiplicator_var = UNITY_ACCESS_INSTANCED_PROP(Props, _Multiplicator);
        //                        float3 emissive = (((_FizzColor_var.rgb * _node_5484_var.rgb) + (_FresnelColor_var.rgb * pow(1.0 - max(0,dot(normalDirection, viewDirection)),_FresnelExp_var))) * _Multiplicator_var);
        //                        float3 finalColor = emissive;
        //                        return fixed4(finalColor,1);
        //                    }
        //                    ENDCG
        //                }


        }
}