// Shader created with Shader Forge v1.40 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.40;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,cpap:True,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33512,y:32692,varname:node_3138,prsc:2|emission-9543-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32098,y:32896,ptovrint:False,ptlb:Fresnel Color,ptin:_FresnelColor,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:True,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_Fresnel,id:8857,x:32289,y:33038,varname:node_8857,prsc:2|EXP-3097-OUT;n:type:ShaderForge.SFN_Multiply,id:1555,x:32468,y:32950,varname:node_1555,prsc:2|A-7241-RGB,B-8857-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3097,x:32109,y:33072,ptovrint:False,ptlb:Fresnel Exp,ptin:_FresnelExp,varname:node_3097,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Tex2d,id:5484,x:32978,y:32574,ptovrint:False,ptlb:node_5484,ptin:_node_5484,varname:node_5484,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-8658-OUT;n:type:ShaderForge.SFN_FragmentPosition,id:1121,x:32098,y:32601,varname:node_1121,prsc:2;n:type:ShaderForge.SFN_Append,id:8658,x:32796,y:32574,varname:node_8658,prsc:2|A-1834-OUT,B-8855-OUT;n:type:ShaderForge.SFN_Vector1,id:1834,x:32711,y:32481,varname:node_1834,prsc:2,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:7174,x:32098,y:32529,ptovrint:False,ptlb:Tex Scale,ptin:_TexScale,varname:node_7174,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:9801,x:32284,y:32601,varname:node_9801,prsc:2|A-7174-OUT,B-1121-Y;n:type:ShaderForge.SFN_Time,id:7185,x:32270,y:32392,varname:node_7185,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2877,x:32439,y:32361,varname:node_2877,prsc:2|A-5454-OUT,B-7185-T;n:type:ShaderForge.SFN_ValueProperty,id:5454,x:32270,y:32316,ptovrint:False,ptlb:Velocity,ptin:_Velocity,varname:node_5454,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Add,id:8855,x:32581,y:32574,varname:node_8855,prsc:2|A-2877-OUT,B-9801-OUT;n:type:ShaderForge.SFN_Add,id:3888,x:33163,y:32762,varname:node_3888,prsc:2|A-5882-OUT,B-1555-OUT;n:type:ShaderForge.SFN_Color,id:2113,x:32934,y:32390,ptovrint:False,ptlb:Fizz Color,ptin:_FizzColor,varname:node_2113,prsc:2,glob:False,taghide:False,taghdr:True,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:5882,x:33172,y:32485,varname:node_5882,prsc:2|A-2113-RGB,B-5484-RGB;n:type:ShaderForge.SFN_ValueProperty,id:8737,x:33042,y:32910,ptovrint:False,ptlb:Multiplicator,ptin:_Multiplicator,varname:node_8737,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:9543,x:33317,y:32838,varname:node_9543,prsc:2|A-3888-OUT,B-8737-OUT;proporder:7241-3097-5484-7174-5454-2113-8737;pass:END;sub:END;*/

Shader "Shader Forge/Holo" {
    Properties {
        [HDR]_FresnelColor ("Fresnel Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        _FresnelExp ("Fresnel Exp", Float ) = 1
        _node_5484 ("node_5484", 2D) = "white" {}
        _TexScale ("Tex Scale", Float ) = 1
        _Velocity ("Velocity", Float ) = 0
        [HDR]_FizzColor ("Fizz Color", Color) = (0.5,0.5,0.5,1)
        _Multiplicator ("Multiplicator", Float ) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma target 3.0
            uniform sampler2D _node_5484; uniform float4 _node_5484_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float4, _FresnelColor)
                UNITY_DEFINE_INSTANCED_PROP( float, _FresnelExp)
                UNITY_DEFINE_INSTANCED_PROP( float, _TexScale)
                UNITY_DEFINE_INSTANCED_PROP( float, _Velocity)
                UNITY_DEFINE_INSTANCED_PROP( float4, _FizzColor)
                UNITY_DEFINE_INSTANCED_PROP( float, _Multiplicator)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _FizzColor_var = UNITY_ACCESS_INSTANCED_PROP( Props, _FizzColor );
                float _Velocity_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Velocity );
                float4 node_7185 = _Time;
                float _TexScale_var = UNITY_ACCESS_INSTANCED_PROP( Props, _TexScale );
                float2 node_8658 = float2(0.0,((_Velocity_var*node_7185.g)+(_TexScale_var*i.posWorld.g)));
                float4 _node_5484_var = tex2D(_node_5484,TRANSFORM_TEX(node_8658, _node_5484));
                float4 _FresnelColor_var = UNITY_ACCESS_INSTANCED_PROP( Props, _FresnelColor );
                float _FresnelExp_var = UNITY_ACCESS_INSTANCED_PROP( Props, _FresnelExp );
                float _Multiplicator_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Multiplicator );
                float3 emissive = (((_FizzColor_var.rgb*_node_5484_var.rgb)+(_FresnelColor_var.rgb*pow(1.0-max(0,dot(normalDirection, viewDirection)),_FresnelExp_var)))*_Multiplicator_var);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
